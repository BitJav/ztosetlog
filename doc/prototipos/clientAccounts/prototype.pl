newClient(U,Name) :-
  newClientOk('NewClient',U,Name)
  ;
  clientAlreadyExists('NewClient',U).

newClientOk(Tn,U,Nname) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog( dom(Clients,DC) &
          U nin DC &
          un(Clients,{[U,Name]},ClientsNew) &
          un(Accounts,{[U,0]},AccountsNew) &
          R = 'ok'),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(r,R).


clientAlreadyExists(Tn,U) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog( dom(Clients,DC) &
          U in DC &
          R = 'clientAlreadyExists' &
          Clients = ClientsNew &
          Accounts = AccountsNew),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(r,R).

transaction(N,M) :-
  transactionOk('Transaction',N,M)
  ;
  accountNotExists('Transaction',N)
  ;
  nullAmount('Transaction',M).

transactionOk(Tn,N,M) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog(dom(Accounts,DA) &
         N in DA &
         M neq 0 &
         apply(Accounts,N,Y) &
         Y1 is Y + M &
         oplus(Accounts,{[N,Y1]},AccountsNew) &
         Clients = ClientsNew &
         R = 'ok'),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(r,R).

accountNotExists(Tn,N) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog( dom(Accounts,DA) &
          N nin DA &
          R = 'accountNotExists' &
          Clients = ClientsNew &
          Accounts = AccountsNew),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(r,R).

nullAmount(Tn,M) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog( M =< 0 & 
          R = 'incorrectAmount' &
          Clients = ClientsNew &
          Accounts = AccountsNew),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(r,R).

accountsInRange(A,B) :-
  accountsInRangeOk('AccountsInRange',A,B)
  ;
  notARange('AccountsInRange',A,B).

accountsInRangeOk(Tn,A,B) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog( A in INT & 
          B in INT &
          A =< B &
          Y = int(A,B) &
          rres(Accounts,Y,Y1) &
          dom(Y1,Result) &
          R = 'ok' &
          Clients = ClientsNew &
          Accounts = AccountsNew),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(result,Result),
  nb_setval(r,R).

notARange(Tn,A,B) :-
  nb_getval(clients,Clients),
  nb_getval(accounts,Accounts),
  setlog( A > B & 
          R = 'notARange' &
          Clients = ClientsNew &
          Accounts = AccountsNew),
  saveFile(Tn,ClientsNew,AccountsNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(clients,ClientsNew),
  nb_setval(accounts,AccountsNew),
  nb_setval(r,R).

