addBirthday(Name,Date) :-
  addBirthdayOk('AddBirthday',Name,Date)
  ;
  nameAlreadyExists('AddBirthday',Name).

addBirthdayOk(Tn,Name,Date) :-
  b_getval(known,Known),
  b_getval(birthday,Birthday),
  setlog( Name nin Known &
          un(Known,{Name},KnownNew) &
          un(Birthday,{[Name,Date]},BirthdayNew)),
  saveFile(Tn,KnownNew,BirthdayNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(known,KnownNew),
  nb_setval(birthday,BirthdayNew).

nameAlreadyExists(Tn,Name) :-
  b_getval(known,Known),
  b_getval(birthday,Birthday),
  setlog( Name in Known &
          Known = KnownNew &
          Birthday = BirthdayNew),
  saveFile(Tn,KnownNew,BirthdayNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(known,KnownNew),
  nb_setval(birthday,BirthdayNew).

findBirthday(Name) :-
  findBirthdayOk('FindBirthday',Name)
  ;
  notAFriend('FindBirthday',Name).

findBirthdayOk(Td,Name) :-
  b_getval(known,Known),
  b_getval(birthday,Birthday),
  setlog( Name in Known &
          apply(Birthday,Name,Date)),
  saveFile(Tn,KnownNew,BirthdayNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(known,KnownNew),
  nb_setval(birthday,BirthdayNew),
  nb_setval(date,Date).

notAFriend(Tn,Name) :-
  b_getval(known,Known),
  b_getval(birthday,Birthday),
  setlog( Name nin Known &
          Known = KnownNew &
          Birthday = BirthdayNew),
  saveFile(Tn,KnownNew,BirthdayNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(known,KnownNew),
  nb_setval(birthday,BirthdayNew).

remind(Today) :-
  b_getval(known,Known),
  b_getval(birthday,Birthday),
  setlog( rres(Birthday,{Today},N1) &
          ran(N1,Today) &
          Known = KnownNew &
          Birthday = BirthdayNew),
  saveFile(Tn,KnownNew,BirthdayNew),
  nb_getval(view,V),
  updateState(V),
  nb_setval(known,KnownNew),
  nb_setval(birthday,BirthdayNew),
  nb_setval(today,Today).

