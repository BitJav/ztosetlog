package rwrules;

/**
 * Interface that abstracts a rewrite rule
 */
interface RWRule{
    /**
     * Returns the regular expression that result of add to the original expression
     * the alternatives ways of writing
     * @param originalExpr
     * @return the generated regular expression 
     */
    String rewrite(String originalExpr);
} 