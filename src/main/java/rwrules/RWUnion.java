package rwrules;

import czt.SpecUtils;
import czt.UniqueZLive;
import net.sourceforge.czt.animation.eval.ZLive;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.parser.z.ParseUtils;
import net.sourceforge.czt.session.SectionManager;
import net.sourceforge.czt.session.StringSource;
import net.sourceforge.czt.z.ast.Pred;
import czt.visitors.FuncApplExtractor;
import rwrules.z.RegExUtils;

import java.util.List;

final class RWUnion implements RWRuleOperator {
    RWUnion() {
        operator = "\\cup";
    }

    public String getOperator() {
        return operator;
    }

    public String rewrite(String originalExpr) {
        String[] parts = originalExpr.split(";");
        String alternativeExprs = "";
        List<Term> terms = null;
        FuncApplExtractor fExtractor = new FuncApplExtractor(operator);
        ZLive zLive = UniqueZLive.getInstance();
        SectionManager manager = zLive.getSectionManager();
        for (String param : parts) {
            //IntersectionExtractor iExtractor = new IntersectionExtractor();
            String leftOperand;
            String rightOperand;
            try {
                Pred parsedPred = ParseUtils.parsePred(new StringSource(param), zLive.getCurrentSection(), zLive.getSectionManager());
                terms = parsedPred.accept(fExtractor);
            } catch (Exception e) {
                System.out.println(e);
            }
            for (Term auxTerm : terms) {
                String strTerm = SpecUtils.termToLatex(auxTerm);
                String operands[] = strTerm.split("cup ");
                leftOperand = operands[0].substring(0, operands[0].length() - 2).trim();
                rightOperand = operands[1].trim();
                leftOperand = RegExUtils.addEscapeCharacters(leftOperand);
                rightOperand = RegExUtils.addEscapeCharacters(rightOperand);
                String oldExpr = leftOperand + " \\\\cup " + rightOperand;
                String newExpr = rightOperand + " \\\\cup " + leftOperand;
                alternativeExprs = alternativeExprs + ";" + param.replaceAll(oldExpr, newExpr);
            }
        }
        return originalExpr + alternativeExprs;
    }

    private String operator;
}