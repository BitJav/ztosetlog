package principal;

import net.sourceforge.czt.session.FileSource;
import net.sourceforge.czt.session.Key;
import net.sourceforge.czt.session.SectionManager;
import net.sourceforge.czt.session.Source;
import net.sourceforge.czt.typecheck.z.ErrorAnn;
import net.sourceforge.czt.typecheck.z.TypeCheckUtils;
import net.sourceforge.czt.z.ast.Spec;
import java.io.*;
import java.util.List;

/**
 * Created by jmesuro on 04/05/16.
 */
final class SpecLoader {
    private Spec spec;

    Spec getSpec() {
         return spec;
    }

    void loadspec(String specFileName) {
        PrintWriter output = new PrintWriter(System.out, true);
        try {

            output.println("Loading specification..");

            String texFileToReadName;
            File originalTexFile;

            originalTexFile = new File(specFileName);
            texFileToReadName = "temp/" + originalTexFile.getName() + ".tmp";
            // A copy of the original file is made, in order to avoid the
            // access restriction from other programs
            makeDir("temp");
            if (!copyFile(specFileName, texFileToReadName))
                return;
            String s = System.getProperty("user.home") + "/ztosetlog/";
            FileSource source = new FileSource(new File(s + texFileToReadName));

            SectionManager manager = new SectionManager();
            manager.put(new Key(texFileToReadName, Source.class), source);


            // The specification is loaded and set in the controller
            spec = (Spec) manager.get(new Key(texFileToReadName, Spec.class));

            List<? extends ErrorAnn> errors =
                    TypeCheckUtils.typecheck(spec, manager, true);
            if (errors.size() > 0) {
                output.println("Specification has not been "
                        + "loaded because it has type errors.");
                for (ErrorAnn error : errors) {
                    String errorStr = error.toString();
                    errorStr = errorStr.substring(errorStr.indexOf(","));
                    errorStr = specFileName + errorStr;
                    output.println(errorStr + "\n");
                }
                System.exit(0);
            }

        } catch (Exception e) {
            output.println("Specification has not been loaded because it has syntax errors.");
            e.printStackTrace();
        }

    }


    /**
     * This method copies the content of the file, whose name is equals to the
     * first specified string, into the file whose name is equals to the second
     * specified string.
     */
    private boolean copyFile(String originalFileName, String newFileName) {
        try {


            FileWriter writer = new FileWriter(newFileName);
            PrintWriter printer = new PrintWriter(writer);
            FileReader reader = new FileReader(originalFileName);
            BufferedReader in = new BufferedReader(reader);
            String line;


            while ((line = in.readLine()) != null) {
                printer.println(line);
            }


            printer.flush();
            printer.close();
            writer.close();
            in.close();
            reader.close();

            return true;

        } catch (FileNotFoundException e) {
            System.out.println("The file " + originalFileName + " could not be found.");
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    private void makeDir(String nameDir){
        File theDir = new File(nameDir);

        if (!theDir.exists()) {

            try{
                theDir.mkdir();
            }
            catch(SecurityException se){
                //handle it
            }
        }
    }

}
