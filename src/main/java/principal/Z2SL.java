package principal;

import net.sourceforge.czt.z.ast.*;
import setLog.SetLogIO;
import translationEngine.slogToZ.SLogToZTranslator;
import translationEngine.zToSlog.ZToSLogTranslator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by joaquin on 31/01/17 z2setlog.
 */
public final class Z2SL {
    private Map<RefExpr, Expr> varExprMap;
    private String setlogInput;
    private StringBuilder setlogOutput = new StringBuilder();
    private String slOutErr;

    private List<Map<RefExpr, Expr>> varExprMapList = new ArrayList<Map<RefExpr, Expr>>();
    private List<String> setlogInputList = new ArrayList<String>();
    private List<StringBuilder> setlogOutputList = new ArrayList<StringBuilder>();



    private List<String> slOutErrList;

//    public List<AxPara> run(List<AxPara> axParaList,Spec spec) throws IOException {
//        Map<RefExpr, Expr> varExprMap;
//        String setlogInput;
//        StringBuilder setlogOutput = new StringBuilder();
//        String slOutErr;
//
//
//        System.err.close();
//        AxPara axPara1 = null;
//        List<AxPara> axParaList1 = new ArrayList<AxPara>();
//
//        try {
//            ZToSLogTranslator zToSLogTranslator = new ZToSLogTranslator();
//            SLogToZTranslator sLogToZTranslator = new SLogToZTranslator();
//
//            for (AxPara axPara : axParaList){
//                zToSLogTranslator.runForZCase(axPara,spec);
//                setlogInput = zToSLogTranslator.printTranslationOut();
//                setlogInputList.add(setlogInput);
//            }
//
//            slOutErrList = SetLogIO.getInstance().runSetLog(setlogInputList,setlogOutputList);
//
//            int i = 0;
//            for (AxPara axPara : axParaList){
//                if (slOutErrList.get(i).equals("OK")){
//
//                    axPara1 = sLogToZTranslator.buildAxPara(setlogOutputList.get(i).toString(), axPara);
//                    axParaList1.add(axPara1);
//                    varExprMap = sLogToZTranslator.getVarExprMap();
//                    varExprMapList.add(varExprMap);
//                }
//                else if (slOutErrList.get(i).equals("FALSE")){
//
//                }
//                i++;
//            }
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//            System.exit(0);
//        }
//        return axParaList1;
//
//    }

    public AxPara run(AxPara axPara,Spec spec) throws IOException {
        System.err.close();
        AxPara axPara1 = null;

        try {
            ZToSLogTranslator zToSLogTranslator = new ZToSLogTranslator();
            SLogToZTranslator sLogToZTranslator = new SLogToZTranslator();

            zToSLogTranslator.runForZCase(axPara,spec);
            setlogInput = zToSLogTranslator.printTranslationOut();
            slOutErr = SetLogIO.getInstance().runSetLog(setlogInput,setlogOutput);
            if (slOutErr.equals("OK")){
                axPara1 = sLogToZTranslator.buildAxPara(setlogOutput.toString(), axPara);
                varExprMap = sLogToZTranslator.getVarExprMap();
            }
            else if (slOutErr.equals("FALSE")){

            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
        return axPara1;

    }



    public void runForPrototipos(AxPara axPara,Spec spec) throws IOException {
        System.err.close();

        try {
            ZToSLogTranslator zToSLogTranslator = new ZToSLogTranslator();
            zToSLogTranslator.runForPrototipo(axPara,spec);
            setlogInput = zToSLogTranslator.printTranslationOut();
            slOutErr = SetLogIO.getInstance().runSetLog(setlogInput,setlogOutput);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }

    }

    public String getSlOutErr(){
        return slOutErr;
    }

    public Map<RefExpr, Expr> getVarExprMap(){
        return varExprMap;
    }

    public List<Map<RefExpr, Expr>> getVarExprMapList() {
        return varExprMapList;
    }

    public List<String> getSlOutErrList() {
        return slOutErrList;
    }

    public String getSetLogCodePP(){
        return "\n*** Setlog input ***\n" + setlogInput + "\n\n" + "*** SetlogOutput ***\n " + slOutErr + " ***\n" + setlogOutput + "\n";
    }
    public String getSetLogCode(){
        return setlogInput.toString() + "\n";
    }
}