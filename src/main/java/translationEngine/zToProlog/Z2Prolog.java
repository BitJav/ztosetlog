package translationEngine.zToProlog;

import czt.visitors.*;
import net.sourceforge.czt.z.ast.AxPara;
import net.sourceforge.czt.z.ast.Spec;
import translationEngine.zToProlog.prototipoState.PrototiposState;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by joaquin on 17/07/17 z2setlog.
 */
public final class Z2Prolog {


    public void run(Spec spec) {
        // Se llena la colección con los nombres de las operaciones de la especificación
        OpNamesExtractor opNamesExtractor = new OpNamesExtractor(spec);
        spec.accept(opNamesExtractor);
        Collection<String> opNamesRep = opNamesExtractor.getOpNames();

        // para esquemas iniciales
        InclDeclDeltaXiExtractor inclDeclDeltaXiExtractor = new InclDeclDeltaXiExtractor();
        Collection<String> deltaXiList = spec.accept(inclDeclDeltaXiExtractor);

        opNamesRep.addAll(deltaXiList);

        // Se crea el mapa que va de los nombres a los AxPara de las operaciones
        AxParaExtractor axParaExtractor = new AxParaExtractor(opNamesRep);
        Map<String, AxPara> opsAxPara = spec.accept(axParaExtractor);


        //OBTENGO LAS VARIABLES POR ESQUEMA; FALTA HACERLO RECURSIVO.
        VarDeclExtractorByOps varDeclExtractorByOps = new VarDeclExtractorByOps(spec);
        spec.accept(varDeclExtractorByOps);
        Map<String,Map<String,String>> varDeclByOps =  varDeclExtractorByOps.getOps();


        PrototiposState.setVarDeclExtractorByOps(varDeclByOps);

        String prologOutPut = PrologPrinter.printOps(spec, opsAxPara);

        System.out.println(prologOutPut);

        //Imprimo en un archivo para testing SACAR PARA FASTEST
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(".//spec.out"));
            bw.write(prologOutPut);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
