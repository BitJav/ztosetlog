package translationEngine.zToProlog;

import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.AxParaVisitor;
import net.sourceforge.czt.z.visitor.RefExprVisitor;
import principal.Z2SL;
import translationEngine.zToProlog.prototipoState.PrototiposState;

import java.io.IOException;
import java.util.*;

/**
 * Created by joaquin on 27/07/17 ztosetlog.
 */
final class SchPrinter implements
        AxParaVisitor<String>,
        RefExprVisitor<String> {


    private String opName; //Nombre de la operación compuesta, es decir la que está del lado izquierdo
    // A = B \lor C entonces opName = "A" ; es nombre CZT
    private Set<String> inputVars; // son nombres prolog
    private Map<String, String> outputVars; // son nombres prolog
    private Spec spec;

    SchPrinter(Spec sl, String opName) {
        this.opName = opName;
        this.inputVars = new HashSet<String>();
        this.outputVars = new HashMap<String, String>();
        this.spec = sl;
    }

    public String visitRefExpr(RefExpr refExpr) {
        //si es un esquema de estado delta o xi
        if (refExpr.getZName().getId().equals("deltaxi"))
            return "";

        String name = "refExpr";
        if (!refExpr.getMixfix() && refExpr.getZExprList().isEmpty()) //Reference
            name = refExpr.getZName().toString();

        AxPara op = PrototiposState.getOp(name);
        if (op == null)
            return "";

        //Si es una operación que no fue visitada, la visito
        String opInExpr = "";
        if (!PrototiposState.alreadyVisited(name)) {
            opInExpr = PrologPrinter.printAxPara(name, op);
        }

        Set<String> nameInputVars = PrototiposState.getInputVars(name);
        inputVars.addAll(nameInputVars);
        outputVars.putAll(PrototiposState.getOutputVars(name));

        return PrologUtils.buildSubOpHorSentence(name, opName, nameInputVars);

    }

    public String visitAxPara(AxPara axPara) {

        Decl decl = axPara.getZSchText().getZDeclList().get(0);
        StringBuilder inclDecls = new StringBuilder("");
        if (decl instanceof ConstDecl) {
            ConstDecl constDecl = (ConstDecl) decl;
            SchExpr schExpr = (SchExpr) constDecl.getExpr();
            ZSchText zSchText1 = schExpr.getZSchText();
            ZDeclList zDeclList1 = zSchText1.getZDeclList();
            for (Decl d : zDeclList1) {
                if (d instanceof InclDecl) {
                    RefExpr refExpr = (RefExpr) ((InclDecl) d).getExpr();
                    String inclDecl = refExpr.accept(this);
                    if (!inclDecl.equals(""))
                        inclDecls.append("\t").append(inclDecl).append(",\n");
                }
            }
        }


        StringBuilder spre = new StringBuilder();
        StringBuilder s = new StringBuilder("\tsetlog( ");
        StringBuilder spos = new StringBuilder();

        Z2SL z2SL = new Z2SL();
        // tiene que ir antes de la corrida de la traducción porque ahí se crea el nuevo Estado para el esquema actual
        // donde se guarda una entrada opName -> opState
        PrototiposState.setOpName(opName);
        try {
            z2SL.runForPrototipos(axPara, spec);


            s.append(z2SL.getSetLogCode());
        } catch (IOException e) {
            e.printStackTrace();
            s.append(z2SL.getSetLogCode());
        }

        String setlog = s.toString(); //traduccion setlog
        String aux = s.toString().substring(0, setlog.lastIndexOf("\n")); //le saco el último enter
        setlog = aux.replaceAll("\n", "\n\t\t") + "),\n"; //le agrego los tabs


        Map<String, String> stateVars = PrototiposState.getZStateSlVar(opName);
        Map<String, String> statePrimed = PrototiposState.getPrimedVars(opName);
        Map<String, String> outVars = PrototiposState.getOutputVars(opName);

        for (String zvar : stateVars.keySet())
            spre.append("\tnb_getval(").append(zvar).append(",").append(stateVars.get(zvar)).append("),\n");

        spos.append("\tsaveFile(Tn");
        for (String slVar : statePrimed.values())
            spos.append(",").append(slVar);
        spos.append("),\n");
        spos.append("\tnb_getval(view,V),\n");
        spos.append("\tupdateState(V),\n");


        outputVars.putAll(outVars);

        HashMap<String, String> outAndPrimed = new HashMap<String, String>();
        outAndPrimed.putAll(outputVars);
        outAndPrimed.putAll(statePrimed);

        for (String zvar : outAndPrimed.keySet())
            spos.append("\tnb_setval(").append(zvar).append(",").append(outAndPrimed.get(zvar)).append("),\n");

        spos.replace(spos.length()-2,spos.length()-1,".");

        // construyo el predicado, operación le llamo por ejemplo AdBirdayOk(Tn,Name, Know) :-

        //Cambiar obtener las input var desde el texto por obtenerlas desde la traduccion de zetlog,
        Set<String> zInputVars = PrototiposState.getInputVars(opName);
        inputVars.addAll(zInputVars);

        String opSentence = PrologUtils.buildOpSentence(opName, inputVars);

        return opSentence + ":-\n" + spre.toString() + inclDecls + setlog + spos.toString() + "\n";
    }
}