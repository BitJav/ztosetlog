package translationEngine.zToProlog;

import net.sourceforge.czt.z.ast.*;
import translationEngine.zToProlog.prototipoState.PrototiposState;

import java.util.Map;

/**
 * Created by joaquin on 15/07/17 z2setlog.
 */
final class PrologPrinter {

    private static Map<String, AxPara> opsAxPara;
    private static Spec spec;

    static String printOps(Spec sp, Map<String, AxPara> ops) {
        spec = sp;
        opsAxPara = ops;

        StringBuilder s = new StringBuilder();
        PrototiposState.setOpsAxPara(opsAxPara);
        for (String name : opsAxPara.keySet()) {
            s.append(printAxPara(name, opsAxPara.get(name)));
        }

        return s.toString();
    }

    static String printAxPara(String opName, AxPara sch) {
        StringBuilder s = new StringBuilder();
        String schBox = sch.getBox().name();

        if (schBox.equals("OmitBox")) { // si es horizontal
            SchHorPrinter schHorPrinter = new SchHorPrinter(opName);
            s.append(sch.accept(schHorPrinter));
        } else if (schBox.equals("SchBox")) { // si es una operacion normal
            SchPrinter schPrinter = new SchPrinter(spec, opName);
            s.append(sch.accept(schPrinter));
        }
        return s.toString();

    }


}
