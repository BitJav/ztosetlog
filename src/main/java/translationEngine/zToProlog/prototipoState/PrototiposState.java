package translationEngine.zToProlog.prototipoState;

import czt.visitors.VarDeclExtractorByOps;
import net.sourceforge.czt.z.ast.AxPara;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by joaquin on 01/08/17 ztosetlog.
 */


public final class PrototiposState {

    //Acá se guarda el estado de la última operación
    private static String lastOp;
    // mapea Esquema Z a sus variables de entrada y de estado, además indica las operaciones que fueron traducidas
    // es decir si op1 tiene una entrada en este mapa entonces ya fue traducida.
    private static Map<String,OpVars> opsVars = new HashMap<String, OpVars>();
    //Necesitamos tener todos las operaciones para visitarlas recursivamente
    private static Map<String, AxPara> opsAxPara;

    private static  Map<String,Map<String,String>> varDeclByOps;

    public static void setVarDeclExtractorByOps( Map<String,Map<String,String>> vars) {
        varDeclByOps = vars;
    }



    public static Map<String,String> getZVarSlVar(String opName){
        if (opsVars.containsKey(opName))
            return opsVars.get(opName).zVarSlVar;

        return null;
    }

    public static Map<String,String> getPrimedVars(String opName){
        if (opsVars.containsKey(opName))
            return opsVars.get(opName).zStateSlPrimed;
        return null;
    }

    public static Set<String> getInputVars(String opName){
        Set<String> inputVars = new HashSet<String>();
        if (opsVars.containsKey(opName))
            for (String zvar : opsVars.get(opName).zVarSlVar.keySet())
                if (zvar.endsWith("?"))
                    inputVars.add(opsVars.get(opName).zVarSlVar.get(zvar));

        return inputVars;
    }

    public static Map<String,String> getOutputVars(String opName){
        if (opsVars.containsKey(opName))
            return opsVars.get(opName).zOutSlVar;
        return null;
    }

    public static Map<String,String> getZStateSlVar(String opName){
        if (opsVars.containsKey(opName))
            return opsVars.get(opName).zStateSlVar;
        return null;
    }


    public static void newSch(Map<String,String> zVarSlVar, Map<String,String> zStateVarZPrimedVar) {
        OpVars opVars = new OpVars();
        opVars.zVarSlVar = zVarSlVar;

        // Viene zStateVar -> ZStatePrimedVar y guardo
        // zStateVar -> SlStatePrologVar
        for(String var : zStateVarZPrimedVar.keySet())
            if (varDeclByOps.get(lastOp).containsKey(var))
                opVars.zStateSlPrimed.put(var, zVarSlVar.get(zStateVarZPrimedVar.get(var)));


        for (String zvar : opVars.zVarSlVar.keySet())
            if (varDeclByOps.get(lastOp).containsKey(zvar))
                if (!zvar.endsWith("?") && !zvar.endsWith("!") && !zvar.endsWith("′"))

                        opVars.zStateSlVar.put(zvar, opVars.zVarSlVar.get(zvar));

        for (String zvar : opVars.zVarSlVar.keySet())
            if (varDeclByOps.get(lastOp).containsKey(zvar))
                if (zvar.endsWith("!"))
                    // le saco el !
                    opVars.zOutSlVar.put(zvar.substring(0,zvar.length()-1), opVars.zVarSlVar.get(zvar));


        opVars.zVarSlVar.putAll(opVars.zOutSlVar);
        opsVars.put(lastOp, opVars);

    }

    public static void setOpName(String opName) {
        lastOp = opName;
    }

    public static boolean alreadyVisited(String opName){
        return opsVars.containsKey(opName);
    }

    public static void setOpsAxPara(Map<String, AxPara> opsAxPara) {
        PrototiposState.opsAxPara = opsAxPara;
    }

    public static AxPara getOp(String opName){
        if (opsAxPara.containsKey(opName))
            return opsAxPara.get(opName);
        return null;
    }
}
