package translationEngine.zToSlog.writer;

/**
 * Created by jmesuro on 04/05/16.
 */
public final class ZSchTextWriter implements Writer{

    private StringBuffer translation = new StringBuffer();

    public String line(String x){
        if (x != null && !x.equals(""))
            return x + " & \n";
        return null;
    }

    public void addTranslationLine(String line){
        if (line != null && !line.equals(""))
            translation.append(line).append(" & \n");
    }

    public void addTranslationAtom(String atom) {
        if (atom != null && !atom.equals(""))
            translation.append(atom);
    }

    public String getTranslation() {
        if (translation != null && translation.length() == 0)
            return "";
        // le saco el último "& \n"
        assert translation != null;
        return translation.toString().substring(0,translation.length()-4);
    }

    public void resetTranslation() {
        translation = new StringBuffer();
    }
}
