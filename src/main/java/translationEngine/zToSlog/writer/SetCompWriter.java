package translationEngine.zToSlog.writer;

import java.util.List;

/**
 * Created by joaquin on 22/08/16.
 */
public final class SetCompWriter implements Writer{

    private StringBuffer translation = new StringBuffer();

    public String line(String x){
        if (x != null && !x.equals(""))
            return x + " & ";
        return null;
    }

    public void addTranslationLine(String line){
        if (line != null && !line.equals(""))
            translation.append(line).append(" & ");
    }


    public void addTranslationAtom(String atom) {
        if (atom != null && !atom.equals(""))
            translation.append(atom);
    }


    public String getTranslation() {
        if (translation != null && translation.length() == 0)
            return "";
        // le saco el último "& \n"
        assert translation != null;
        return translation.toString().substring(0,translation.length()-3);
    }

    public void resetTranslation() {
        translation = new StringBuffer();
    }
}
