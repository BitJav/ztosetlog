package translationEngine.zToSlog;

import czt.PrimedVarAdder;
import czt.visitors.*;
import czt.SpecUtils;
import czt.visitors.SpecCleaner;
import net.sourceforge.czt.z.ast.*;
import translationEngine.slogToZ.ToZState;
import translationEngine.zToProlog.prototipoState.PrototiposState;
import translationEngine.zToSlog.writer.Writer;
import translationEngine.zToSlog.writer.ZSchTextWriter;

import java.util.*;


/**
 * Created by jmesuro on 07/05/16.
 */
final public class ZToSLogTranslator {

    private Writer writer;
    private AxPara axPara; // variable para correr Principal, Principal es para probar el proyecto standalone
    private Spec spec;

    //sume que hay un solo ax para esta versión del traductor
    private AxPara getAxPara(ZParaList zParaList) {
        AxPara axPara = null;
        for (Para para : zParaList) {
            if (para instanceof AxPara) {
                axPara = (AxPara) para;
            }
        }
        return axPara;
    }

    public void run(Spec spec) throws Exception {

        initSpecState(spec);

        ZParaList zParaList = SpecUtils.getZParaList(spec);
        axPara = getAxPara(zParaList);

        SchExpr schExpr = SpecUtils.getAxParaSchExpr(axPara);
        if (schExpr == null) {
            throw new Exception("Could not find test class schema.");
        }
        ZSchText zSchText = schExpr.getZSchText();

        writer = new ZSchTextWriter();
        zSchText.accept(new GroundTypeDefinitionPrinter(writer));
        zSchText.accept(new ZSchTextPrinter(writer));

        fillToZState();
    }




    private void run() throws Exception{


        SchExpr schExpr = SpecUtils.getAxParaSchExpr(this.axPara);
        if (schExpr == null) {
            throw new Exception("Could not find test class schema.");
        }
        ZSchText zSchText = schExpr.getZSchText();

        writer = new ZSchTextWriter();
        zSchText.accept(new GroundTypeDefinitionPrinter(writer));
        zSchText.accept(new ZSchTextPrinter(writer));
    }

    public void runForZCase(AxPara axPara,Spec spec) throws Exception {
        this.axPara = axPara;
        this.spec = (Spec) spec.accept(new SpecCleaner(axPara,spec));
        initSpecState(spec);

        run();
        fillToZState(axPara);
    }

    public void runForPrototipo(AxPara axPara,Spec spec) throws Exception {
        this.axPara = axPara;
        this.spec = (Spec) spec.accept(new SpecCleaner(axPara,spec));
        initSpecState(spec);

        // preproc primedVars
        preprocForPrototipos();

        run();
        fillPrototiposState();
    }


    private void fillPrototiposState(){
        Map<String,String> vars = ToSlogState.getAllVarSlVar();
        Set<String> zVars = ToSlogState.getZVars();
        Map<String,String> zVarSlVar = new HashMap<String, String>();
        for (String zVar: zVars){
            if (vars.containsKey(zVar))
                zVarSlVar.put(zVar,vars.get(zVar));
        }
        PrototiposState.newSch(zVarSlVar,ToSlogState.getzStateVarZPrimedVar());

    }

//Difiere de RulesUtils.setExtention en que este no guada el tipo, es solo
    //la generación de una lista con corchetes,

private String setEnumerationType(List<String> elems) {
    String salida;
    if (elems.isEmpty())
        salida = "{}";
    else {
        StringBuilder set = new StringBuilder("{");
        for (String elem : elems)
            set.append(elem).append(",");
        salida =  set.substring(0, set.length() - 1) + "}";
    }
    return salida;
}

    /* Llena el estado para la traducción de SL a Z
     * */
    private void fillToZState(){
        Map<String,List<String>> ft = ToSlogState.getFreeTypes();
        Set<String> keys = ft.keySet();
        for (String key : keys) {
            for (String elem : ft.get(key))
                ToZState.tipos.put(elem,key);
            ToZState.tipos.put(key, "EnumerationType:" + key + ":" + setEnumerationType(ft.get(key)));
        }
        Set<String> bt = ToSlogState.getBasicTypes();
        for (String type : bt)
            ToZState.tipos.put(type,"BasicType:"+type);


        Map<String,String> zVars = new HashMap<String, String>();
        for (String zvar : ToSlogState.getZVars())
            zVars.put(zvar,null);

        ToZState.zVars = zVars;
        ToZState.zNames.putAll(ToSlogState.getAllVarSlVar());
        ToZState.zNames.putAll(ToSlogState.getZCteSlCte());


        Map<String,String> types = new HashMap<String, String>(ToSlogState.getAllVarZType());


        for (String var : ToSlogState.getSchTypes().keySet())
            types.put(var, ToZState.getSchName());

        List<String> l;
        for (String var : ToSlogState.getSchTypes().keySet()){
            l = new ArrayList<String>();
            for (String vari : ToSlogState.getSchTypes().get(var).keySet())
                l.add(vari + ":" + types.get(vari));
            types.put(types.get(var),"SchemaType:" + types.get(var) + ":" + l );
        }
        ToZState.tipos.putAll(types);
    }


    /* Llena el estado para la traducción de SL a Z
     * */

    private void fillToZState(AxPara axPara){

        Map<String,String> allVarZType = axPara.accept(new VarDeclExtractor());
        Set<String> axParaDeclVars = allVarZType.keySet();


        Map<String,List<String>> ft = ToSlogState.getFreeTypes();
        Set<String> keys = ft.keySet();
        for (String key : keys) {
            for (String elem : ft.get(key))
                ToZState.tipos.put(elem,key);
            ToZState.tipos.put(key, "EnumerationType:" + key + ":" + setEnumerationType(ft.get(key)));
        }
        Set<String> bt = ToSlogState.getBasicTypes();
        for (String type : bt)
            ToZState.tipos.put(type,"BasicType:"+type);


        Map<String,String> zVars = new HashMap<String, String>();
        for (String zvar : ToSlogState.getZVars())
            if (axParaDeclVars.contains(zvar))
                zVars.put(zvar,null);


        ToZState.zVars = zVars;
        ToZState.zNames.putAll(ToSlogState.getAllVarSlVar());
        ToZState.zNames.putAll(ToSlogState.getZCteSlCte());


        Map<String,String> types = new HashMap<String, String>(ToSlogState.getAllVarZType());


        for (String var : ToSlogState.getSchTypes().keySet())
            types.put(var, ToZState.getSchName());

        List<String> l;
        for (String var : ToSlogState.getSchTypes().keySet()){
            l = new ArrayList<String>();
            for (String vari : ToSlogState.getSchTypes().get(var).keySet())
                l.add(vari + ":" + types.get(vari));
            types.put(types.get(var),"SchemaType:" + types.get(var) + ":" + l );
        }
        ToZState.tipos.putAll(types);
    }


    private void toSl(String zName){

        if (!ToSlogState.varMapped(zName)){
            String slName = RulesUtils.toSlVar(zName);
            if (ToSlogState.slVarNameAssigned(slName))
                slName = RulesUtils.newSlVar();

            //porque uso biyección, puede ser que un slName se corresponde con
            //el nombre de una variable z
            if (ToSlogState.varMapped(slName))
                slName = RulesUtils.newSlVar();

            ToSlogState.putAllVarSlVar(zName,slName);
        }

    }

    /*Para los prototipos los esquemas Z pueden y van a contener variables primadas
   * Además, puede que no todas las variables primadas aparezcan en el predicado
   * preprocForPrototipos hace que aparezcan ej:
   * A = [x,y : \num, x = y] --> A = [x,y : \num, x = y \and x' = x \and y' = y]
   * Es decir agrega una igualdad en el predicado para todas las variables que
   * tienen pinta de ser variables de estado y su estado primado no está en el mismo.
   * */
    private void preprocForPrototipos(){
        Map<String,String> primedVars = new HashMap<String,String>(); // aca solo van las uqwe no aparecen en el esquema explícitamente
        Map<String,String> allPrimedVars = new HashMap<String,String>(); // todas las variables de estado y su primas

        String zVarPrimed = null;
        String slVarPrimed = null;
        Set<String> zVars = ToSlogState.getZVars();

        for (String zVar: zVars){
            if (RulesUtils.isStateNoPrimed(zVar)){
                zVarPrimed = RulesUtils.makePrimed(zVar);
                if (! ToSlogState.getZVars().contains(zVarPrimed))
                    primedVars.put(zVar,zVarPrimed); // Solo agfego las ue no estan en el predicado del esquema

                slVarPrimed = RulesUtils.toSlVar(zVarPrimed);
                ToSlogState.putAllVarSlVar(zVarPrimed,slVarPrimed);
                ToSlogState.addSlVar(slVarPrimed);
                allPrimedVars.put(zVar,zVarPrimed);
            }
        }

        zVars.addAll(allPrimedVars.values());
        ToSlogState.setZVars(zVars);
        ToSlogState.setzStateVarZPrimedVar(allPrimedVars);

        AxPara newAxPara = (new PrimedVarAdder(axPara,primedVars)).buildNewAxPara();
        axPara = newAxPara;
    }
    private void initSpecState(Spec spec){

        ToSlogState.init();
        ToZState.init();

        //preproc constDecls
        ToSlogState.setConstDecls(spec.accept(new ConstDeclExtractor()));
        //spec = (Spec) spec.accept(new RefExprReplacer(SpecState.getConstDecls())); //unfoldeo los constDecl

        //preproc basicTypes
        Set<String> basicTypes  = spec.accept(new BasicTypeNamesExtractor());
        for (String basicType: basicTypes)
            toSl(basicType);
        ToSlogState.setBasicTypes(basicTypes);

        //preproc freeTypes and constants
        Map<String,List<String>> freeTypes = spec.accept(new FreeTypeExtractor());
        for (String zname : freeTypes.keySet())
            toSl(zname);
        ToSlogState.setFreeTypes(freeTypes);

        //preproc sequences
        Set<String> seqs = spec.accept(new SeqExtractor());
        Set<String> seqsSl = new HashSet<String>();
        for (String zname : seqs){
            toSl(zname);
            seqsSl.add(ToSlogState.getVarSlName(zname));
        }
        ToSlogState.setSequences(seqsSl);

        //preproc zVar <-> ZType

        VarDeclExtractor vde = new VarDeclExtractor();
        Map<String,String> allVarZType = spec.accept(vde);
        for (String zvar : allVarZType.keySet())
            toSl(zvar);
        ToSlogState.setAllVarZType(allVarZType);
        ToSlogState.setAllVarCZTExprType(spec.accept(new VarCZTDeclExtractor()));
        ToSlogState.setZVars(ToSlogState.getAllVarCZTExprType().keySet());//al principio son todas Z

        //preproc SchTypes
        ToSlogState.setSchTypes(spec.accept(new SchTypeExtractor()));


        //preproc constants, las constantes son los elementos de los freetypes, no hay otra!!!
        Set<String> constants = new HashSet<String>();
        for (Map.Entry<String,List<String>> entry : freeTypes.entrySet())
            constants.addAll(entry.getValue());

        //preproc slCtes
        Map<String,String> zctes = new HashMap<String, String>();
        for (String zcte : constants)
            if (!zctes.containsKey(zcte)){
                String zcteSl = RulesUtils.toSlCte(zcte);
                zctes.put(zcte, zcteSl);
                zctes.put(zcteSl, zcte);
        }
        ToSlogState.setZSlCte(zctes);

    }

    /*Sólo se usa para correr Principal
    * */
    public AxPara getAxPara(Spec spec){
        ZParaList zParaList = SpecUtils.getZParaList(spec);
        axPara = getAxPara(zParaList);
        return axPara;
    }

    public String printTranslationOut() {
        return writer.getTranslation();
    }



}