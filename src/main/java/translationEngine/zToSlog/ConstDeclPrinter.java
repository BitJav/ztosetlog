package translationEngine.zToSlog;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.ConstDecl;
import net.sourceforge.czt.z.visitor.ConstDeclVisitor;
import translationEngine.zToSlog.writer.Writer;

/**
 * Created by jmesuro on 05/08/16.
 *
 * */
public class ConstDeclPrinter implements
        TermVisitor<String>,
        ConstDeclVisitor<String>

{
    private Writer writer;

    ConstDeclPrinter(Writer writer){
        this.writer = writer;
    }

    public String  visitTerm(Term term) {
        Object[] array = term.getChildren();
        for (final Object object : array) {
            if (object instanceof Term) {
                ((Term) object).accept(this);
            }
        }
        return "";
    }

    public String visitConstDecl(ConstDecl constDecl) {
        String var = constDecl.getZName().toString();
        return constDecl.getExpr().accept(new ExprPredPrinter(writer));
    }
}