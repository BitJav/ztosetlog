package translationEngine.zToSlog;

import net.sourceforge.czt.z.ast.Decl;
import net.sourceforge.czt.z.ast.ZSchText;
import net.sourceforge.czt.z.visitor.ZSchTextVisitor;
import translationEngine.zToSlog.writer.Writer;

import java.util.HashSet;

/**
 * Created by jmesuro on 11/07/16.
 */
final class ZSchTextPrinter implements
        ZSchTextVisitor<String>

{

    private Writer writer;

    ZSchTextPrinter(Writer writer){
        this.writer = writer;
    }
    public String visitZSchText(ZSchText zSchText) {

        ToSlogState.pushSlVars(new HashSet<String>());

        DeclPrinter dp = new DeclPrinter(writer);
        for (Decl decl : zSchText.getZDeclList())
            decl.accept(dp);

        if (zSchText.getPred() != null)
            zSchText.getPred().accept(new PredPrinter(writer));

        return writer.getTranslation();
    }


}
