package translationEngine.zToSlog;

import net.sourceforge.czt.z.ast.*;

import java.util.*;

/**
 * Created by jmesuro on 08/06/16.
 */
final class ToSlogState {

    private static Set<String> printedSlVars = new HashSet<String>();
    private static Map<String,Expr> constDecls = new HashMap<String, Expr>();
    private static Map<String,List<String>> freeTypes = new HashMap<String, List<String>>();
    private static Set<String> basicTypes = new HashSet<String>();
    private static Set<String> sequences = new HashSet<String>();
    private static Map<String,Map<String,String>> schTypes = new HashMap<String,Map<String,String>>();

    //Ojo zVarType.keyset() e != zVarSlVar.keyset()
    //porque en el segundo están todas las variables como por ejemplo
    //las variables de basictype [A] ; en el primero solo están las variables de la declaración x :\num
    //obvio tiene elementos en común

    private static Set<String> zVars = new HashSet<String>(); //variables del esquema z original
    private static Map<String,String> zCteSlCte = new BiHashMap<String>(); // se va llenando
    private static Map<String,String> allVarSlVar = new BiHashMap<String>(); // se va llenando, incluye auxiliares
    private static Map<String,Expr> allVarCZTExprType = new HashMap<String,Expr>(); //incluye las variables auxiliares, se va llenando
    private static Map<String, String> allVarZType = new HashMap<String, String>(); // incluye las variables auxiliares, se va llenando
    private static Map<Expr,String> exprAuxVar = new HashMap<Expr,String>(); // es para no repetir definiciones
    //por ej el caso en z .. 1 + a = 0 \\ 1 + a = 0 entonces AUX1  = 1 + a  y AUX2 = 1 + a
    private static Map<String,String> zStateVarZPrimedVar = new HashMap<String,String>(); // guarda las variables y su primada en z
    // si tenía x' entonces la guardo como xNew

    //Para no repetir opreaciones setlog por ej si tengo
    // A : \seq MDATA \\ ( 1 ... 43 ) \cap \dom A \neq \{ \} \\
    // aparece dom(A,int(1,AUX333)) y dom(A,AUX123) o sea la definicion del domino de A dos veces.
    //Entonces hago lo siguiente: cambio la definicion de seq por: dom(A,AUX1) & AUX1 = int(1,AUX2)
    // y guardo la entrada en el map  (dom,A,AUX1) y en la segunda definicion de dom no la traduzco porque
    //ya tengo a AUX1
    private static Map<String,Map<String,String>> slOpAuxVar = new HashMap<String,Map<String,String>>();

    //para setComp
    // stackSlVars.peek() in allVarSlVar
    //en el primero están solo las del entorno actual, en el segundo están todas
    final private static Stack<Set<String>> stackSlVars = new Stack<Set<String>>();
    static int nvar = -1;
    static int auxvar = -1;
    static int ncte = 99;

    static void init(){
        printedSlVars.clear();
        constDecls.clear();
        freeTypes.clear();
        basicTypes.clear();
        sequences.clear();
        schTypes.clear();
        zVars.clear();
        zCteSlCte.clear();
        allVarSlVar.clear();
        allVarCZTExprType.clear();
        allVarZType.clear();
        exprAuxVar.clear();
        slOpAuxVar.clear();
        stackSlVars.clear();
        zStateVarZPrimedVar.clear();
    }

    static void setZVars(Set<String> zVars){
        ToSlogState.zVars.addAll(zVars);
    }
    static void addZVar(String zVar){
        zVars.add(zVar);
    }
    static Set<String> getZVars(){return zVars;}

    static void setConstDecls(Map<String,Expr> constDecls){
        ToSlogState.constDecls.putAll(constDecls);
    }
    static Expr getExprFromConsDecl(String constDecl){
        return constDecls.get(constDecl);
    }
    static boolean isConstDecl(String var){
        return constDecls.containsKey(var);
    }
    static boolean isNat(String type){
        if (type.equals(RulesUtils.CZTNAT))
            return true;
        if (constDecls == null)
            return false;
        Expr expr = constDecls.get(type);
        return expr instanceof RefExpr && ((RefExpr) expr).getZName().toString().equals(RulesUtils.CZTNAT);
    }
    static boolean isNat1(String type) {
        if (type.equals(RulesUtils.CZTNAT1))
            return true;
        if (constDecls == null)
            return false;
        Expr expr = constDecls.get(type);
        return expr instanceof RefExpr && ((RefExpr) expr).getZName().toString().equals(RulesUtils.CZTNAT1);
    }
    static boolean isInt(String type) {
        if (type.equals(RulesUtils.CZTINT))
            return true;
        if (constDecls == null)
            return false;
        Expr expr = constDecls.get(type);
        return expr instanceof RefExpr && ((RefExpr) expr).getZName().toString().equals(RulesUtils.CZTINT);
    }

    static Map<String,List<String>> getFreeTypes(){
        return freeTypes;
    }
    static void setFreeTypes(Map<String, List<String>> freeTypes) {
        ToSlogState.freeTypes.putAll(freeTypes);
    }
    static List<String> getFreeTypeElems(String type){
        return freeTypes.get(type);
    }
    static boolean isFreeType(String type){
        return freeTypes != null && freeTypes.containsKey(type);
    }

    static void setBasicTypes(Set<String> basicTypes) {
        ToSlogState.basicTypes.addAll(basicTypes);
    }
    static Set<String> getBasicTypes(){
        return basicTypes;
    }
    static boolean isBasicType(String type) {
        return basicTypes != null && basicTypes.contains(type);
    }

    static void setSequences(Set<String> sequences) {
        ToSlogState.sequences.addAll(sequences);
    }
    static boolean isSequence(String name) {
        return sequences != null && sequences.contains(name);
    }

    static void putAllVarSlVar(String zvar, String slvar){
        allVarSlVar.put(zvar,slvar);
    }
    static Map<String,String> getAllVarSlVar(){
        return allVarSlVar;
    }
    static String getVarSlName(String zvar){
        return allVarSlVar.get(zvar);
    }
    static boolean varMapped(String var){
        return allVarSlVar.containsKey(var);
    }
    static boolean slVarNameAssigned(String slvar){
        return allVarSlVar.containsValue(slvar);
    }

    static void setZSlCte(Map<String,String> zSlCte){
        zCteSlCte.putAll(zSlCte);
    }
    static Map<String,String> getZCteSlCte(){
        return zCteSlCte;
    }
    static String getSlName(String ref){
        if (zCteSlCte.containsKey(ref))
            return zCteSlCte.get(ref);
        else if(allVarSlVar.containsKey(ref))
            return allVarSlVar.get(ref);
        return ref;
    }
    static String getCteSlName(String zcte){
        return zCteSlCte.get(zcte);
    }

    static void pushSlVars(Set<String> slVrs){
        stackSlVars.push(slVrs);
    }
    static Set<String> popSlVarsCreated(){
        return stackSlVars.pop();
    }
    static void addSlVar(String var){
        if (!stackSlVars.isEmpty())
            stackSlVars.peek().add(var);
    }

    static Map<String, String> getAllVarZType() {
        return allVarZType;
    }
    static void setAllVarZType(Map<String, String> zVarAndAuxVarZType) {
        ToSlogState.allVarZType = zVarAndAuxVarZType;
    }
    static void putVarToAllVarZType(String var, String type){
        ToSlogState.allVarZType.put(var,type);
    }
    static String getZTypeAuxVarZType(String var){return ToSlogState.allVarZType.get(var);}

    static Map<String, Expr> getAllVarCZTExprType() {
        return allVarCZTExprType;
    }
    static void setAllVarCZTExprType(Map<String, Expr> zVarCZTExpr) {
        ToSlogState.allVarCZTExprType = zVarCZTExpr;
    }
    //devuelve el tipo en forma de Expr de una sl variable.
    static Expr getSlExprType(String var){
        Expr expr = allVarCZTExprType.get(allVarSlVar.get(var));
        if (expr == null)
            return TypeUtils.num();
        else
            return expr;
    }
    static void putCZTExprType(String var, Expr type){
        allVarCZTExprType.put(var,type);
    }

    static Map<String,Map<String,String>> getSchTypes() {
        return schTypes;
    }
    static void setSchTypes(Map<String,Map<String,String>> schTypes) {
        ToSlogState.schTypes = schTypes;
    }

    static boolean isPrintedSlVar(String line) {
        return printedSlVars.contains(line);
    }
    static void addPrintedSlVar (String line) {
        printedSlVars.add(line);
    }

    static String getAuxVarExprAuxVar(Expr expr){
        return exprAuxVar.get(expr);
    }
    static void putExprAuxVar(Expr expr, String auxVar){
        exprAuxVar.put(expr, auxVar);
    }

    static boolean containsAuxVarSlOpAuxVar(String op, String var){
        if (slOpAuxVar.containsKey(op))
            if (slOpAuxVar.get(op).containsKey(var))
                return true;
        return false;
    }
    static String getAuxVarSlOpAuxVar(String op, String var){
        if (slOpAuxVar.containsKey(op))
            if (slOpAuxVar.get(op).containsKey(var))
                return slOpAuxVar.get(op).get(var);
        return null;
    }
    static void putSlOpAuxVar(String op, String var, String auxVar){
        Map<String,String> map;
        if (slOpAuxVar.containsKey(op))
            map = slOpAuxVar.get(op);
        else
            map = new HashMap<String, String>();
        map.put(var,auxVar);
        slOpAuxVar.put(op,map);
    }

    static Map<String,String> getzStateVarZPrimedVar() {
        return zStateVarZPrimedVar;
    }

    static void setzStateVarZPrimedVar(Map<String,String> primedZVarSlVar) {
        ToSlogState.zStateVarZPrimedVar = primedZVarSlVar;
    }
}