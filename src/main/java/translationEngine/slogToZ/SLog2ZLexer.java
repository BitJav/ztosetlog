// Generated from SLog2Z.g4 by ANTLR 4.6

package translationEngine.slogToZ;
import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SLog2ZLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, NAME=24, CTE=25, 
		NUM=26, NL=27, WS=28, SKIP_=29;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "NAME", "CTE", "NUM", 
		"NL", "WS", "SKIP_"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'_CONSTR'", "'='", "'['", "','", "']'", "'set('", "')'", "'list('", 
		"'integer('", "'dom('", "'ran('", "'disj('", "'comp('", "'un('", "'neq'", 
		"'nin'", "'int'", "'('", "'{'", "'/'", "'}'", "'|'", "'-'", null, null, 
		null, "'\n'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"NAME", "CTE", "NUM", "NL", "WS", "SKIP_"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


		Map<String,StringBuilder> slVars = new HashMap();
		Map<String,String> zNames = new HashMap();
		Map<String,String> tipos = new HashMap();
		Map<String,String> zVars = new HashMap();
		Map<String,String> valoresProhibidos = new HashMap();
		List<String> varNoGenerar = new LinkedList<String>();
		ConstantCreator cc;

		public Map<String,StringBuilder> getSlvars(){
			return slVars;
		}

		public Map<String,String> getZVars(){
			return zVars;
		}

		public ConstantCreator getCC(){
			return cc;
		}

		public void loadTablas(Map<String,String> zVars, Map<String,String> tipos, Map<String,String> zNames){
			this.zNames = zNames;
			this.tipos = tipos;
			this.zVars = zVars;
			cc = new ConstantCreator(tipos,slVars,zNames,valoresProhibidos);
		}

		private void printHashMap(Map map){
			Iterator iterator = map.keySet().iterator();
			String key,value;
			while (iterator.hasNext()) {
			   key = iterator.next().toString();
			   if (map.get(key) == null)
				   value = "nullc";
			   else
				   value = map.get(key).toString();
			   System.out.println(key + " = " + value);
			}
		}

		private void printHashMap2(Map<String,String[]> map){
			Iterator<String> iterator = map.keySet().iterator();
			String key;	String[] value;
			while (iterator.hasNext()) {
			   key = iterator.next().toString();
			   if (map.get(key) == null){
				   System.out.println(key + " = " + "nullc");
				   continue;
			   }
			   else{
				   value = map.get(key);
				   System.out.print(key + " = ");
				   for (int i = 0; i<value.length;i++)
					   System.out.print(value[i] + ",");
				   System.out.println();
			   }
			}
		}

		private void preprocesarConstraint(){
		// por que pueden venir variables Z, que solo aparezcan en constraint, no hay que llenarlas en ZVarFiller
			// por que ahi ya pueden tener valor erroneor ej constraint [V neq [], list(V)], con list V se le da valors
				if(valoresProhibidos != null){
				Iterator<String> it = valoresProhibidos.keySet().iterator();
				String var,tipo;
				StringBuilder valor;
				while (it.hasNext()) {
					var = it.next().toString();
					if (zNames != null && zNames.get(var)!=null){
						tipo = tipos.get(zNames.get(var));
						DefaultMutableTreeNode nodo = SLogUtils.toTreeNorm(tipo);
						valor = new StringBuilder(cc.getCte(var,nodo));
						if(slVars != null)
							slVars.put(var, valor);
						}
					}
				}
		}

		private void llenarZVars(){
			Iterator iterator = slVars.keySet().iterator();
			String slname,zname,valor;
			while (iterator.hasNext()) {
				slname = iterator.next().toString();
				if (slVars.get(slname)!=null){
					valor = slVars.get(slname).toString();
					zname = zNames.get(slname);
					if (zVars.containsKey(zname)){
						zVars.put(zname,valor);
					}
				}
			}
		}



	public SLog2ZLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SLog2Z.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 27:
			WS_action((RuleContext)_localctx, actionIndex);
			break;
		case 28:
			SKIP__action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			skip();
			break;
		}
	}
	private void SKIP__action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\37\u00b5\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3"+
		"\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3"+
		"\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\7\31\u0098\n\31\f\31\16\31\u009b"+
		"\13\31\3\32\3\32\7\32\u009f\n\32\f\32\16\32\u00a2\13\32\3\33\6\33\u00a5"+
		"\n\33\r\33\16\33\u00a6\3\34\3\34\3\35\6\35\u00ac\n\35\r\35\16\35\u00ad"+
		"\3\35\3\35\3\36\3\36\3\36\3\36\2\2\37\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21"+
		"\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30"+
		"/\31\61\32\63\33\65\34\67\359\36;\37\3\2\7\4\2C\\aa\6\2\62;C\\aac|\5\2"+
		"//\62;c|\5\2\62;C\\c|\5\2\13\13\17\17\"\"\u00b8\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\3=\3\2\2\2\5E\3"+
		"\2\2\2\7G\3\2\2\2\tI\3\2\2\2\13K\3\2\2\2\rM\3\2\2\2\17R\3\2\2\2\21T\3"+
		"\2\2\2\23Z\3\2\2\2\25c\3\2\2\2\27h\3\2\2\2\31m\3\2\2\2\33s\3\2\2\2\35"+
		"y\3\2\2\2\37}\3\2\2\2!\u0081\3\2\2\2#\u0085\3\2\2\2%\u0089\3\2\2\2\'\u008b"+
		"\3\2\2\2)\u008d\3\2\2\2+\u008f\3\2\2\2-\u0091\3\2\2\2/\u0093\3\2\2\2\61"+
		"\u0095\3\2\2\2\63\u009c\3\2\2\2\65\u00a4\3\2\2\2\67\u00a8\3\2\2\29\u00ab"+
		"\3\2\2\2;\u00b1\3\2\2\2=>\7a\2\2>?\7E\2\2?@\7Q\2\2@A\7P\2\2AB\7U\2\2B"+
		"C\7V\2\2CD\7T\2\2D\4\3\2\2\2EF\7?\2\2F\6\3\2\2\2GH\7]\2\2H\b\3\2\2\2I"+
		"J\7.\2\2J\n\3\2\2\2KL\7_\2\2L\f\3\2\2\2MN\7u\2\2NO\7g\2\2OP\7v\2\2PQ\7"+
		"*\2\2Q\16\3\2\2\2RS\7+\2\2S\20\3\2\2\2TU\7n\2\2UV\7k\2\2VW\7u\2\2WX\7"+
		"v\2\2XY\7*\2\2Y\22\3\2\2\2Z[\7k\2\2[\\\7p\2\2\\]\7v\2\2]^\7g\2\2^_\7i"+
		"\2\2_`\7g\2\2`a\7t\2\2ab\7*\2\2b\24\3\2\2\2cd\7f\2\2de\7q\2\2ef\7o\2\2"+
		"fg\7*\2\2g\26\3\2\2\2hi\7t\2\2ij\7c\2\2jk\7p\2\2kl\7*\2\2l\30\3\2\2\2"+
		"mn\7f\2\2no\7k\2\2op\7u\2\2pq\7l\2\2qr\7*\2\2r\32\3\2\2\2st\7e\2\2tu\7"+
		"q\2\2uv\7o\2\2vw\7r\2\2wx\7*\2\2x\34\3\2\2\2yz\7w\2\2z{\7p\2\2{|\7*\2"+
		"\2|\36\3\2\2\2}~\7p\2\2~\177\7g\2\2\177\u0080\7s\2\2\u0080 \3\2\2\2\u0081"+
		"\u0082\7p\2\2\u0082\u0083\7k\2\2\u0083\u0084\7p\2\2\u0084\"\3\2\2\2\u0085"+
		"\u0086\7k\2\2\u0086\u0087\7p\2\2\u0087\u0088\7v\2\2\u0088$\3\2\2\2\u0089"+
		"\u008a\7*\2\2\u008a&\3\2\2\2\u008b\u008c\7}\2\2\u008c(\3\2\2\2\u008d\u008e"+
		"\7\61\2\2\u008e*\3\2\2\2\u008f\u0090\7\177\2\2\u0090,\3\2\2\2\u0091\u0092"+
		"\7~\2\2\u0092.\3\2\2\2\u0093\u0094\7/\2\2\u0094\60\3\2\2\2\u0095\u0099"+
		"\t\2\2\2\u0096\u0098\t\3\2\2\u0097\u0096\3\2\2\2\u0098\u009b\3\2\2\2\u0099"+
		"\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a\62\3\2\2\2\u009b\u0099\3\2\2"+
		"\2\u009c\u00a0\t\4\2\2\u009d\u009f\t\5\2\2\u009e\u009d\3\2\2\2\u009f\u00a2"+
		"\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\64\3\2\2\2\u00a2"+
		"\u00a0\3\2\2\2\u00a3\u00a5\4\62;\2\u00a4\u00a3\3\2\2\2\u00a5\u00a6\3\2"+
		"\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\66\3\2\2\2\u00a8\u00a9"+
		"\7\f\2\2\u00a98\3\2\2\2\u00aa\u00ac\t\6\2\2\u00ab\u00aa\3\2\2\2\u00ac"+
		"\u00ad\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00af\3\2"+
		"\2\2\u00af\u00b0\b\35\2\2\u00b0:\3\2\2\2\u00b1\u00b2\7^\2\2\u00b2\u00b3"+
		"\7^\2\2\u00b3\u00b4\b\36\3\2\u00b4<\3\2\2\2\7\2\u0099\u00a0\u00a6\u00ad"+
		"\4\3\35\2\3\36\3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}