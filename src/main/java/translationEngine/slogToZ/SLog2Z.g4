grammar SLog2Z;

@header {
package translationEngine.slogToZ;
import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;
}

@members {
	Map<String,StringBuilder> slVars = new HashMap();
	Map<String,String> zNames = new HashMap();
	Map<String,String> tipos = new HashMap();
	Map<String,String> zVars = new HashMap();
	Map<String,String> valoresProhibidos = new HashMap();
	List<String> varNoGenerar = new LinkedList<String>();
	ConstantCreator cc;

	public Map<String,StringBuilder> getSlvars(){
		return slVars;
	}

	public Map<String,String> getZVars(){
		return zVars;
	}

	public ConstantCreator getCC(){
		return cc;
	}

	public void loadTablas(Map<String,String> zVars, Map<String,String> tipos, Map<String,String> zNames){
		this.zNames = zNames;
		this.tipos = tipos;
		this.zVars = zVars;
		cc = new ConstantCreator(tipos,slVars,zNames,valoresProhibidos);
	}

	private void printHashMap(Map map){
		Iterator iterator = map.keySet().iterator();
		String key,value;
		while (iterator.hasNext()) {
		   key = iterator.next().toString();
		   if (map.get(key) == null)
			   value = "nullc";
		   else
			   value = map.get(key).toString();
		   System.out.println(key + " = " + value);
		}
	}

	private void printHashMap2(Map<String,String[]> map){
		Iterator<String> iterator = map.keySet().iterator();
		String key;	String[] value;
		while (iterator.hasNext()) {
		   key = iterator.next().toString();
		   if (map.get(key) == null){
			   System.out.println(key + " = " + "nullc");
			   continue;
		   }
		   else{
			   value = map.get(key);
			   System.out.print(key + " = ");
			   for (int i = 0; i<value.length;i++)
				   System.out.print(value[i] + ",");
			   System.out.println();
		   }
		}
	}

	private void preprocesarConstraint(){
	// por que pueden venir variables Z, que solo aparezcan en constraint, no hay que llenarlas en ZVarFiller
		// por que ahi ya pueden tener valor erroneor ej constraint [V neq [], list(V)], con list V se le da valors
			if(valoresProhibidos != null){
			Iterator<String> it = valoresProhibidos.keySet().iterator();
			String var,tipo;
			StringBuilder valor;
			while (it.hasNext()) {
				var = it.next().toString();
				if (zNames != null && zNames.get(var)!=null){
					tipo = tipos.get(zNames.get(var));
					DefaultMutableTreeNode nodo = SetLogUtils.toTreeNorm(tipo);
					valor = new StringBuilder(cc.getCte(var,nodo));
					if(slVars != null)
						slVars.put(var, valor);
					}
				}
			}
	}

	private void llenarZVars(){
		Iterator iterator = slVars.keySet().iterator();
		String slname,zname,valor;
		while (iterator.hasNext()) {
			slname = iterator.next().toString();
			if (slVars.get(slname)!=null){
				valor = slVars.get(slname).toString();
				zname = zNames.get(slname);
				if (zVars.containsKey(zname)){
					zVars.put(zname,valor);
				}
			}
		}
	}

}

lineas
	:	constr NL {
			preprocesarConstraint();
		}
		(seqIgual NL?)+
		{
			llenarZVars();
		}
	;

constr
	:	'_CONSTR' '=' '[' (restr(','restr)*)? ']' ','
	;

restr
locals [StringBuilder valor]
@init{$restr::valor = new StringBuilder();}
	: 'set(' expr ')'
	    {
	        $restr::valor.append("{}");
	        slVars.put($expr.text,$restr::valor);
	    }
	| 'list(' expr ')'
	    {
	        $restr::valor.append("[]");
	        slVars.put($expr.text,$restr::valor);
	    }
	| 'integer(' expr ')'
	    {
	        $restr::valor.append("0");
	        slVars.put($expr.text,$restr::valor);
        }
	| 'dom(' r = expr ',' q = expr ')'
	    {
	        $restr::valor.append("{}");
	        slVars.put($q.text,$restr::valor);
	        slVars.put($r.text,$restr::valor);
        }
	| 'ran('  r = expr ',' q = expr ')'
	    {
            $restr::valor.append("{}");
            slVars.put($q.text,$restr::valor);
            slVars.put($r.text,$restr::valor);
        }
    | 'disj('r = expr ',' q = expr ')'
        {
            $restr::valor.append("{}");
            slVars.put($q.text,$restr::valor);
            slVars.put($r.text,$restr::valor);
        }
    | 'comp(' r = expr ',' t = NAME ',' tt = expr ')'
        {
            $restr::valor.append("{}");
            slVars.put($r.text,$restr::valor);
            slVars.put($tt.text,$restr::valor);
        }
    | 'comp(' t = NAME ',' r = expr ',' tt = expr ')'
        {
            $restr::valor.append("{}");
            slVars.put($r.text,$restr::valor);
            slVars.put($tt.text,$restr::valor);
        }
    | 'comp(' q = expr ',' r = expr ',' tt = expr ')'
         {
             $restr::valor.append("{}");
             slVars.put($r.text,$restr::valor);
             slVars.put($q.text,$restr::valor);
             slVars.put($tt.text,$restr::valor);
         }
    | 'un(' q = expr ',' r = expr ',' tt = expr ')'
             {
                 $restr::valor.append("{}");
                 slVars.put($r.text,$restr::valor);
                 slVars.put($q.text,$restr::valor);
                 slVars.put($tt.text,$restr::valor);
             }

	| (a = NAME 'neq' b = NAME)
		{
			String var1 = $a.text;
			String var2 = $b.text;
			String s = valoresProhibidos.get(var1);

			if (s!=null && !s.contains(var2))
				valoresProhibidos.put(var1,s.concat("," + var2));
			else{
				s = new String(var2);
				valoresProhibidos.put(var1, s);
				}

			s = valoresProhibidos.get(var2);
			if (s!=null && !s.contains(var1))
				valoresProhibidos.put(var2,s.concat("," + var1));
			else{
				s = new String(var1);
				valoresProhibidos.put(var2, s);
				}
		}
	| (NAME 'neq' exprCte | exprCte 'neq' NAME)
		{
			String var = $NAME.text;
			String cte = $exprCte.text;
			String s = valoresProhibidos.get(var);
			if (s!=null && !s.contains(cte))
				valoresProhibidos.put(var,s.concat("," + cte));
			else{
				s = new String(cte);
				valoresProhibidos.put(var, s);
				}
		}
	| (NAME 'neq' expr | expr 'neq' NAME)
		{
			varNoGenerar.add($NAME.text);
			slVars.put($NAME.text,new StringBuilder("ValueNotAssigned"));
		}

    | r = expr 'nin' a = NAME
        {
            $restr::valor.append("{}");
            slVars.put($a.text,$restr::valor);
        }
	;




//otrctr: '<'* '>'* '*'* '&'* '+'* '.'*;

seqIgual
locals [StringBuilder valor]
@init{$seqIgual::valor = new StringBuilder();}
	:	(v1=NAME {slVars.put($v1.text,$seqIgual::valor);} '=' v2=expr {slVars.put($v2.text,$seqIgual::valor);} ',')+
		{
			String zname = zNames.get($v1.text);
			String tipo = tipos.get(zname);
			String var = $v2.valor;

			if (varNoGenerar.contains(var) || varNoGenerar.contains($v1.text))
				$seqIgual::valor.append("ValueNotAssigned");

			else if (   !$seqIgual::(valor.toString()).contains("ValueNotAssigned") &&
			            !varNoGenerar.contains(var) &&
			            !zname.startsWith("\\n") &&
			            !tipo.startsWith("BasicType") &&
			            !tipo.startsWith("EnumerationType") &&
			            !tipo.startsWith("SchemaType") )

				$seqIgual::valor.append(cc.getCte(var,SetLogUtils.toTreeNorm(tipo)));

		 }
	;

expr
returns [String valor]
	:	CTE {$valor = $CTE.text;}
	|   NAME {$valor = $NAME.text;}
	|   'int' '(' a=(NAME|CTE) ',' b=(NAME|CTE) ')' {$valor = "int(" + $a.text + "," + $b.text + ")";} 
	|	'{' {$valor = "{";}
	    ( (','{$valor = $valor + ",";})?  e=expr {$valor = $valor + $e.valor;})*
	    ('/' NAME)*
	    '}' {$valor = $valor + "}";}
	|   '[' {$valor = "[";}
	    ( (','{$valor = $valor + ",";})?  e=expr {$valor = $valor + $e.valor;})*
	    ('|' expr)*
	    ']' {$valor = $valor + "]";} 
	|	'-' expr {$valor = "-" + $valor ;}
	;

exprCte
returns [String valor]
	:	CTE {$valor = $CTE.text;}
	|   'int' '(' a=(NAME|CTE) ',' b=(NAME|CTE) ')' {$valor = "int(" + $a.text + "," + $b.text + ")";} 
	|	'{' {$valor = "{";}
	    ( (','{$valor = $valor + ",";})?  e=exprCte {$valor = $valor + $e.valor;})*
	    ('/' NAME)*
	    '}' {$valor = $valor + "}";}
	|   '[' {$valor = "[";}
	    ( (','{$valor = $valor + ",";})?  e=exprCte {$valor = $valor + $e.valor;})*
	    ('|' exprCte)*
	    ']' {$valor = $valor + "]";} 
	|	'-' exprCte {$valor = "-" + $valor ;}
	;

		

NAME:	('_'|'A'..'Z' ) ('_'|'a'..'z' | 'A'..'Z' |'0'..'9')*;
CTE:    ('-'|'a'..'z' |'0'..'9') ('a'..'z' | 'A'..'Z' |'0'..'9')*;
NUM:	'0'..'9'+ ;

NL:	'\n';
WS: 	(' '|'\t'|'\r')+ {skip();} ;
SKIP_:	'\\' '\\' {skip();} ;