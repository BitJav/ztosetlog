// Generated from SLog2Z.g4 by ANTLR 4.6

package translationEngine.slogToZ;
import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SLog2ZParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, NAME=24, CTE=25, 
		NUM=26, NL=27, WS=28, SKIP_=29;
	public static final int
		RULE_lineas = 0, RULE_constr = 1, RULE_restr = 2, RULE_seqIgual = 3, RULE_expr = 4, 
		RULE_exprCte = 5;
	public static final String[] ruleNames = {
		"lineas", "constr", "restr", "seqIgual", "expr", "exprCte"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'_CONSTR'", "'='", "'['", "','", "']'", "'set('", "')'", "'list('", 
		"'integer('", "'dom('", "'ran('", "'disj('", "'comp('", "'un('", "'neq'", 
		"'nin'", "'int'", "'('", "'{'", "'/'", "'}'", "'|'", "'-'", null, null, 
		null, "'\n'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"NAME", "CTE", "NUM", "NL", "WS", "SKIP_"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SLog2Z.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		Map<String,StringBuilder> slVars = new HashMap();
		Map<String,String> zNames = new HashMap();
		Map<String,String> tipos = new HashMap();
		Map<String,String> zVars = new HashMap();
		Map<String,String> valoresProhibidos = new HashMap();
		List<String> varNoGenerar = new LinkedList<String>();
		ConstantCreator cc;

		public Map<String,StringBuilder> getSlvars(){
			return slVars;
		}

		public Map<String,String> getZVars(){
			return zVars;
		}

		public ConstantCreator getCC(){
			return cc;
		}

		public void loadTablas(Map<String,String> zVars, Map<String,String> tipos, Map<String,String> zNames){
			this.zNames = zNames;
			this.tipos = tipos;
			this.zVars = zVars;
			cc = new ConstantCreator(tipos,slVars,zNames,valoresProhibidos);
		}

		private void printHashMap(Map map){
			Iterator iterator = map.keySet().iterator();
			String key,value;
			while (iterator.hasNext()) {
			   key = iterator.next().toString();
			   if (map.get(key) == null)
				   value = "nullc";
			   else
				   value = map.get(key).toString();
			   System.out.println(key + " = " + value);
			}
		}

		private void printHashMap2(Map<String,String[]> map){
			Iterator<String> iterator = map.keySet().iterator();
			String key;	String[] value;
			while (iterator.hasNext()) {
			   key = iterator.next().toString();
			   if (map.get(key) == null){
				   System.out.println(key + " = " + "nullc");
				   continue;
			   }
			   else{
				   value = map.get(key);
				   System.out.print(key + " = ");
				   for (int i = 0; i<value.length;i++)
					   System.out.print(value[i] + ",");
				   System.out.println();
			   }
			}
		}

		private void preprocesarConstraint(){
		// por que pueden venir variables Z, que solo aparezcan en constraint, no hay que llenarlas en ZVarFiller
			// por que ahi ya pueden tener valor erroneor ej constraint [V neq [], list(V)], con list V se le da valors
				if(valoresProhibidos != null){
				Iterator<String> it = valoresProhibidos.keySet().iterator();
				String var,tipo;
				StringBuilder valor;
				while (it.hasNext()) {
					var = it.next().toString();
					if (zNames != null && zNames.get(var)!=null){
						tipo = tipos.get(zNames.get(var));
						DefaultMutableTreeNode nodo = SLogUtils.toTreeNorm(tipo);
						valor = new StringBuilder(cc.getCte(var,nodo));
						if(slVars != null)
							slVars.put(var, valor);
						}
					}
				}
		}

		private void llenarZVars(){
			Iterator iterator = slVars.keySet().iterator();
			String slname,zname,valor;
			while (iterator.hasNext()) {
				slname = iterator.next().toString();
				if (slVars.get(slname)!=null){
					valor = slVars.get(slname).toString();
					zname = zNames.get(slname);
					if (zVars.containsKey(zname)){
						zVars.put(zname,valor);
					}
				}
			}
		}


	public SLog2ZParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LineasContext extends ParserRuleContext {
		public ConstrContext constr() {
			return getRuleContext(ConstrContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(SLog2ZParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(SLog2ZParser.NL, i);
		}
		public List<SeqIgualContext> seqIgual() {
			return getRuleContexts(SeqIgualContext.class);
		}
		public SeqIgualContext seqIgual(int i) {
			return getRuleContext(SeqIgualContext.class,i);
		}
		public LineasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lineas; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).enterLineas(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).exitLineas(this);
		}
	}

	public final LineasContext lineas() throws RecognitionException {
		LineasContext _localctx = new LineasContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_lineas);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(12);
			constr();
			setState(13);
			match(NL);

						preprocesarConstraint();
					
			setState(19); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(15);
				seqIgual();
				setState(17);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NL) {
					{
					setState(16);
					match(NL);
					}
				}

				}
				}
				setState(21); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==NAME );

						llenarZVars();
					
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstrContext extends ParserRuleContext {
		public List<RestrContext> restr() {
			return getRuleContexts(RestrContext.class);
		}
		public RestrContext restr(int i) {
			return getRuleContext(RestrContext.class,i);
		}
		public ConstrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).enterConstr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).exitConstr(this);
		}
	}

	public final ConstrContext constr() throws RecognitionException {
		ConstrContext _localctx = new ConstrContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_constr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			match(T__0);
			setState(26);
			match(T__1);
			setState(27);
			match(T__2);
			setState(36);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__5) | (1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__16) | (1L << T__18) | (1L << T__22) | (1L << NAME) | (1L << CTE))) != 0)) {
				{
				setState(28);
				restr();
				setState(33);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__3) {
					{
					{
					setState(29);
					match(T__3);
					setState(30);
					restr();
					}
					}
					setState(35);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(38);
			match(T__4);
			setState(39);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RestrContext extends ParserRuleContext {
		public StringBuilder valor;
		public ExprContext expr;
		public ExprContext r;
		public ExprContext q;
		public Token t;
		public ExprContext tt;
		public Token a;
		public Token b;
		public Token NAME;
		public ExprCteContext exprCte;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> NAME() { return getTokens(SLog2ZParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SLog2ZParser.NAME, i);
		}
		public ExprCteContext exprCte() {
			return getRuleContext(ExprCteContext.class,0);
		}
		public RestrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_restr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).enterRestr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).exitRestr(this);
		}
	}

	public final RestrContext restr() throws RecognitionException {
		RestrContext _localctx = new RestrContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_restr);
		((RestrContext)getInvokingContext(2)).valor =  new StringBuilder();
		try {
			setState(145);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(41);
				match(T__5);
				setState(42);
				((RestrContext)_localctx).expr = expr();
				setState(43);
				match(T__6);

					        ((RestrContext)getInvokingContext(2)).valor.append("{}");
					        slVars.put((((RestrContext)_localctx).expr!=null?_input.getText(((RestrContext)_localctx).expr.start,((RestrContext)_localctx).expr.stop):null),((RestrContext)getInvokingContext(2)).valor);
					    
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(46);
				match(T__7);
				setState(47);
				((RestrContext)_localctx).expr = expr();
				setState(48);
				match(T__6);

					        ((RestrContext)getInvokingContext(2)).valor.append("[]");
					        slVars.put((((RestrContext)_localctx).expr!=null?_input.getText(((RestrContext)_localctx).expr.start,((RestrContext)_localctx).expr.stop):null),((RestrContext)getInvokingContext(2)).valor);
					    
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(51);
				match(T__8);
				setState(52);
				((RestrContext)_localctx).expr = expr();
				setState(53);
				match(T__6);

					        ((RestrContext)getInvokingContext(2)).valor.append("0");
					        slVars.put((((RestrContext)_localctx).expr!=null?_input.getText(((RestrContext)_localctx).expr.start,((RestrContext)_localctx).expr.stop):null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(56);
				match(T__9);
				setState(57);
				((RestrContext)_localctx).r = expr();
				setState(58);
				match(T__3);
				setState(59);
				((RestrContext)_localctx).q = expr();
				setState(60);
				match(T__6);

					        ((RestrContext)getInvokingContext(2)).valor.append("{}");
					        slVars.put((((RestrContext)_localctx).q!=null?_input.getText(((RestrContext)_localctx).q.start,((RestrContext)_localctx).q.stop):null),((RestrContext)getInvokingContext(2)).valor);
					        slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(63);
				match(T__10);
				setState(64);
				((RestrContext)_localctx).r = expr();
				setState(65);
				match(T__3);
				setState(66);
				((RestrContext)_localctx).q = expr();
				setState(67);
				match(T__6);

				            ((RestrContext)getInvokingContext(2)).valor.append("{}");
				            slVars.put((((RestrContext)_localctx).q!=null?_input.getText(((RestrContext)_localctx).q.start,((RestrContext)_localctx).q.stop):null),((RestrContext)getInvokingContext(2)).valor);
				            slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(70);
				match(T__11);
				setState(71);
				((RestrContext)_localctx).r = expr();
				setState(72);
				match(T__3);
				setState(73);
				((RestrContext)_localctx).q = expr();
				setState(74);
				match(T__6);

				            ((RestrContext)getInvokingContext(2)).valor.append("{}");
				            slVars.put((((RestrContext)_localctx).q!=null?_input.getText(((RestrContext)_localctx).q.start,((RestrContext)_localctx).q.stop):null),((RestrContext)getInvokingContext(2)).valor);
				            slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(77);
				match(T__12);
				setState(78);
				((RestrContext)_localctx).r = expr();
				setState(79);
				match(T__3);
				setState(80);
				((RestrContext)_localctx).t = match(NAME);
				setState(81);
				match(T__3);
				setState(82);
				((RestrContext)_localctx).tt = expr();
				setState(83);
				match(T__6);

				            ((RestrContext)getInvokingContext(2)).valor.append("{}");
				            slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				            slVars.put((((RestrContext)_localctx).tt!=null?_input.getText(((RestrContext)_localctx).tt.start,((RestrContext)_localctx).tt.stop):null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(86);
				match(T__12);
				setState(87);
				((RestrContext)_localctx).t = match(NAME);
				setState(88);
				match(T__3);
				setState(89);
				((RestrContext)_localctx).r = expr();
				setState(90);
				match(T__3);
				setState(91);
				((RestrContext)_localctx).tt = expr();
				setState(92);
				match(T__6);

				            ((RestrContext)getInvokingContext(2)).valor.append("{}");
				            slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				            slVars.put((((RestrContext)_localctx).tt!=null?_input.getText(((RestrContext)_localctx).tt.start,((RestrContext)_localctx).tt.stop):null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(95);
				match(T__12);
				setState(96);
				((RestrContext)_localctx).q = expr();
				setState(97);
				match(T__3);
				setState(98);
				((RestrContext)_localctx).r = expr();
				setState(99);
				match(T__3);
				setState(100);
				((RestrContext)_localctx).tt = expr();
				setState(101);
				match(T__6);

				             ((RestrContext)getInvokingContext(2)).valor.append("{}");
				             slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				             slVars.put((((RestrContext)_localctx).q!=null?_input.getText(((RestrContext)_localctx).q.start,((RestrContext)_localctx).q.stop):null),((RestrContext)getInvokingContext(2)).valor);
				             slVars.put((((RestrContext)_localctx).tt!=null?_input.getText(((RestrContext)_localctx).tt.start,((RestrContext)_localctx).tt.stop):null),((RestrContext)getInvokingContext(2)).valor);
				         
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(104);
				match(T__13);
				setState(105);
				((RestrContext)_localctx).q = expr();
				setState(106);
				match(T__3);
				setState(107);
				((RestrContext)_localctx).r = expr();
				setState(108);
				match(T__3);
				setState(109);
				((RestrContext)_localctx).tt = expr();
				setState(110);
				match(T__6);

				                 ((RestrContext)getInvokingContext(2)).valor.append("{}");
				                 slVars.put((((RestrContext)_localctx).r!=null?_input.getText(((RestrContext)_localctx).r.start,((RestrContext)_localctx).r.stop):null),((RestrContext)getInvokingContext(2)).valor);
				                 slVars.put((((RestrContext)_localctx).q!=null?_input.getText(((RestrContext)_localctx).q.start,((RestrContext)_localctx).q.stop):null),((RestrContext)getInvokingContext(2)).valor);
				                 slVars.put((((RestrContext)_localctx).tt!=null?_input.getText(((RestrContext)_localctx).tt.start,((RestrContext)_localctx).tt.stop):null),((RestrContext)getInvokingContext(2)).valor);
				             
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				{
				setState(113);
				((RestrContext)_localctx).a = match(NAME);
				setState(114);
				match(T__14);
				setState(115);
				((RestrContext)_localctx).b = match(NAME);
				}

							String var1 = (((RestrContext)_localctx).a!=null?((RestrContext)_localctx).a.getText():null);
							String var2 = (((RestrContext)_localctx).b!=null?((RestrContext)_localctx).b.getText():null);
							String s = valoresProhibidos.get(var1);

							if (s!=null && !s.contains(var2))
								valoresProhibidos.put(var1,s.concat("," + var2));
							else{
								s = new String(var2);
								valoresProhibidos.put(var1, s);
								}

							s = valoresProhibidos.get(var2);
							if (s!=null && !s.contains(var1))
								valoresProhibidos.put(var2,s.concat("," + var1));
							else{
								s = new String(var1);
								valoresProhibidos.put(var2, s);
								}
						
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(125);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case NAME:
					{
					setState(118);
					((RestrContext)_localctx).NAME = match(NAME);
					setState(119);
					match(T__14);
					setState(120);
					((RestrContext)_localctx).exprCte = exprCte();
					}
					break;
				case T__2:
				case T__16:
				case T__18:
				case T__22:
				case CTE:
					{
					setState(121);
					((RestrContext)_localctx).exprCte = exprCte();
					setState(122);
					match(T__14);
					setState(123);
					((RestrContext)_localctx).NAME = match(NAME);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}

							String var = (((RestrContext)_localctx).NAME!=null?((RestrContext)_localctx).NAME.getText():null);
							String cte = (((RestrContext)_localctx).exprCte!=null?_input.getText(((RestrContext)_localctx).exprCte.start,((RestrContext)_localctx).exprCte.stop):null);
							String s = valoresProhibidos.get(var);
							if (s!=null && !s.contains(cte))
								valoresProhibidos.put(var,s.concat("," + cte));
							else{
								s = new String(cte);
								valoresProhibidos.put(var, s);
								}
						
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(136);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
				case 1:
					{
					setState(129);
					((RestrContext)_localctx).NAME = match(NAME);
					setState(130);
					match(T__14);
					setState(131);
					expr();
					}
					break;
				case 2:
					{
					setState(132);
					expr();
					setState(133);
					match(T__14);
					setState(134);
					((RestrContext)_localctx).NAME = match(NAME);
					}
					break;
				}

							varNoGenerar.add((((RestrContext)_localctx).NAME!=null?((RestrContext)_localctx).NAME.getText():null));
							slVars.put((((RestrContext)_localctx).NAME!=null?((RestrContext)_localctx).NAME.getText():null),new StringBuilder("ValueNotAssigned"));
						
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(140);
				((RestrContext)_localctx).r = expr();
				setState(141);
				match(T__15);
				setState(142);
				((RestrContext)_localctx).a = match(NAME);

				            ((RestrContext)getInvokingContext(2)).valor.append("{}");
				            slVars.put((((RestrContext)_localctx).a!=null?((RestrContext)_localctx).a.getText():null),((RestrContext)getInvokingContext(2)).valor);
				        
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SeqIgualContext extends ParserRuleContext {
		public StringBuilder valor;
		public Token v1;
		public ExprContext v2;
		public List<TerminalNode> NAME() { return getTokens(SLog2ZParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SLog2ZParser.NAME, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public SeqIgualContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_seqIgual; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).enterSeqIgual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).exitSeqIgual(this);
		}
	}

	public final SeqIgualContext seqIgual() throws RecognitionException {
		SeqIgualContext _localctx = new SeqIgualContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_seqIgual);
		((SeqIgualContext)getInvokingContext(3)).valor =  new StringBuilder();
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(154); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(147);
					((SeqIgualContext)_localctx).v1 = match(NAME);
					slVars.put((((SeqIgualContext)_localctx).v1!=null?((SeqIgualContext)_localctx).v1.getText():null),((SeqIgualContext)getInvokingContext(3)).valor);
					setState(149);
					match(T__1);
					setState(150);
					((SeqIgualContext)_localctx).v2 = expr();
					slVars.put((((SeqIgualContext)_localctx).v2!=null?_input.getText(((SeqIgualContext)_localctx).v2.start,((SeqIgualContext)_localctx).v2.stop):null),((SeqIgualContext)getInvokingContext(3)).valor);
					setState(152);
					match(T__3);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(156); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );

						String zname = zNames.get((((SeqIgualContext)_localctx).v1!=null?((SeqIgualContext)_localctx).v1.getText():null));
						String tipo = tipos.get(zname);
						String var = ((SeqIgualContext)_localctx).v2.valor;

						if (varNoGenerar.contains(var) || varNoGenerar.contains((((SeqIgualContext)_localctx).v1!=null?((SeqIgualContext)_localctx).v1.getText():null)))
							((SeqIgualContext)getInvokingContext(3)).valor.append("ValueNotAssigned");

						else if (   !((SeqIgualContext)getInvokingContext(3)).valor.toString().contains("ValueNotAssigned") &&
						            !varNoGenerar.contains(var) &&
						            !zname.startsWith("\\n") &&
						            !tipo.startsWith("BasicType") &&
						            !tipo.startsWith("EnumerationType") &&
						            !tipo.startsWith("SchemaType") )

							((SeqIgualContext)getInvokingContext(3)).valor.append(cc.getCte(var, SLogUtils.toTreeNorm(tipo)));

					 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public String valor;
		public Token CTE;
		public Token NAME;
		public Token a;
		public Token b;
		public ExprContext e;
		public List<TerminalNode> CTE() { return getTokens(SLog2ZParser.CTE); }
		public TerminalNode CTE(int i) {
			return getToken(SLog2ZParser.CTE, i);
		}
		public List<TerminalNode> NAME() { return getTokens(SLog2ZParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SLog2ZParser.NAME, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_expr);
		int _la;
		try {
			setState(221);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CTE:
				enterOuterAlt(_localctx, 1);
				{
				setState(160);
				((ExprContext)_localctx).CTE = match(CTE);
				((ExprContext)_localctx).valor =  (((ExprContext)_localctx).CTE!=null?((ExprContext)_localctx).CTE.getText():null);
				}
				break;
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(162);
				((ExprContext)_localctx).NAME = match(NAME);
				((ExprContext)_localctx).valor =  (((ExprContext)_localctx).NAME!=null?((ExprContext)_localctx).NAME.getText():null);
				}
				break;
			case T__16:
				enterOuterAlt(_localctx, 3);
				{
				setState(164);
				match(T__16);
				setState(165);
				match(T__17);
				setState(166);
				((ExprContext)_localctx).a = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==NAME || _la==CTE) ) {
					((ExprContext)_localctx).a = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(167);
				match(T__3);
				setState(168);
				((ExprContext)_localctx).b = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==NAME || _la==CTE) ) {
					((ExprContext)_localctx).b = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(169);
				match(T__6);
				((ExprContext)_localctx).valor =  "int(" + (((ExprContext)_localctx).a!=null?((ExprContext)_localctx).a.getText():null) + "," + (((ExprContext)_localctx).b!=null?((ExprContext)_localctx).b.getText():null) + ")";
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 4);
				{
				setState(171);
				match(T__18);
				((ExprContext)_localctx).valor =  "{";
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__16) | (1L << T__18) | (1L << T__22) | (1L << NAME) | (1L << CTE))) != 0)) {
					{
					{
					setState(175);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==T__3) {
						{
						setState(173);
						match(T__3);
						((ExprContext)_localctx).valor =  _localctx.valor + ",";
						}
					}

					setState(177);
					((ExprContext)_localctx).e = expr();
					((ExprContext)_localctx).valor =  _localctx.valor + ((ExprContext)_localctx).e.valor;
					}
					}
					setState(184);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(189);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__19) {
					{
					{
					setState(185);
					match(T__19);
					setState(186);
					match(NAME);
					}
					}
					setState(191);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(192);
				match(T__20);
				((ExprContext)_localctx).valor =  _localctx.valor + "}";
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 5);
				{
				setState(194);
				match(T__2);
				((ExprContext)_localctx).valor =  "[";
				setState(205);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__16) | (1L << T__18) | (1L << T__22) | (1L << NAME) | (1L << CTE))) != 0)) {
					{
					{
					setState(198);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==T__3) {
						{
						setState(196);
						match(T__3);
						((ExprContext)_localctx).valor =  _localctx.valor + ",";
						}
					}

					setState(200);
					((ExprContext)_localctx).e = expr();
					((ExprContext)_localctx).valor =  _localctx.valor + ((ExprContext)_localctx).e.valor;
					}
					}
					setState(207);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(212);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__21) {
					{
					{
					setState(208);
					match(T__21);
					setState(209);
					expr();
					}
					}
					setState(214);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(215);
				match(T__4);
				((ExprContext)_localctx).valor =  _localctx.valor + "]";
				}
				break;
			case T__22:
				enterOuterAlt(_localctx, 6);
				{
				setState(217);
				match(T__22);
				setState(218);
				expr();
				((ExprContext)_localctx).valor =  "-" + _localctx.valor ;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprCteContext extends ParserRuleContext {
		public String valor;
		public Token CTE;
		public Token a;
		public Token b;
		public ExprCteContext e;
		public List<TerminalNode> CTE() { return getTokens(SLog2ZParser.CTE); }
		public TerminalNode CTE(int i) {
			return getToken(SLog2ZParser.CTE, i);
		}
		public List<TerminalNode> NAME() { return getTokens(SLog2ZParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SLog2ZParser.NAME, i);
		}
		public List<ExprCteContext> exprCte() {
			return getRuleContexts(ExprCteContext.class);
		}
		public ExprCteContext exprCte(int i) {
			return getRuleContext(ExprCteContext.class,i);
		}
		public ExprCteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprCte; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).enterExprCte(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SLog2ZListener ) ((SLog2ZListener)listener).exitExprCte(this);
		}
	}

	public final ExprCteContext exprCte() throws RecognitionException {
		ExprCteContext _localctx = new ExprCteContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_exprCte);
		int _la;
		try {
			setState(282);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CTE:
				enterOuterAlt(_localctx, 1);
				{
				setState(223);
				((ExprCteContext)_localctx).CTE = match(CTE);
				((ExprCteContext)_localctx).valor =  (((ExprCteContext)_localctx).CTE!=null?((ExprCteContext)_localctx).CTE.getText():null);
				}
				break;
			case T__16:
				enterOuterAlt(_localctx, 2);
				{
				setState(225);
				match(T__16);
				setState(226);
				match(T__17);
				setState(227);
				((ExprCteContext)_localctx).a = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==NAME || _la==CTE) ) {
					((ExprCteContext)_localctx).a = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(228);
				match(T__3);
				setState(229);
				((ExprCteContext)_localctx).b = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==NAME || _la==CTE) ) {
					((ExprCteContext)_localctx).b = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(230);
				match(T__6);
				((ExprCteContext)_localctx).valor =  "int(" + (((ExprCteContext)_localctx).a!=null?((ExprCteContext)_localctx).a.getText():null) + "," + (((ExprCteContext)_localctx).b!=null?((ExprCteContext)_localctx).b.getText():null) + ")";
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 3);
				{
				setState(232);
				match(T__18);
				((ExprCteContext)_localctx).valor =  "{";
				setState(243);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__16) | (1L << T__18) | (1L << T__22) | (1L << CTE))) != 0)) {
					{
					{
					setState(236);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==T__3) {
						{
						setState(234);
						match(T__3);
						((ExprCteContext)_localctx).valor =  _localctx.valor + ",";
						}
					}

					setState(238);
					((ExprCteContext)_localctx).e = exprCte();
					((ExprCteContext)_localctx).valor =  _localctx.valor + ((ExprCteContext)_localctx).e.valor;
					}
					}
					setState(245);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(250);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__19) {
					{
					{
					setState(246);
					match(T__19);
					setState(247);
					match(NAME);
					}
					}
					setState(252);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(253);
				match(T__20);
				((ExprCteContext)_localctx).valor =  _localctx.valor + "}";
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 4);
				{
				setState(255);
				match(T__2);
				((ExprCteContext)_localctx).valor =  "[";
				setState(266);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__16) | (1L << T__18) | (1L << T__22) | (1L << CTE))) != 0)) {
					{
					{
					setState(259);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==T__3) {
						{
						setState(257);
						match(T__3);
						((ExprCteContext)_localctx).valor =  _localctx.valor + ",";
						}
					}

					setState(261);
					((ExprCteContext)_localctx).e = exprCte();
					((ExprCteContext)_localctx).valor =  _localctx.valor + ((ExprCteContext)_localctx).e.valor;
					}
					}
					setState(268);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(273);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__21) {
					{
					{
					setState(269);
					match(T__21);
					setState(270);
					exprCte();
					}
					}
					setState(275);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(276);
				match(T__4);
				((ExprCteContext)_localctx).valor =  _localctx.valor + "]";
				}
				break;
			case T__22:
				enterOuterAlt(_localctx, 5);
				{
				setState(278);
				match(T__22);
				setState(279);
				exprCte();
				((ExprCteContext)_localctx).valor =  "-" + _localctx.valor ;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\37\u011f\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\2\3\2\3\2\5\2\24\n"+
		"\2\6\2\26\n\2\r\2\16\2\27\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\7\3\"\n\3\f"+
		"\3\16\3%\13\3\5\3\'\n\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\5\4\u0080\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\5\4\u008b\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\u0094\n\4\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\6\5\u009d\n\5\r\5\16\5\u009e\3\5\3\5\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00b2\n\6\3\6\3\6\3\6"+
		"\7\6\u00b7\n\6\f\6\16\6\u00ba\13\6\3\6\3\6\7\6\u00be\n\6\f\6\16\6\u00c1"+
		"\13\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00c9\n\6\3\6\3\6\3\6\7\6\u00ce\n\6"+
		"\f\6\16\6\u00d1\13\6\3\6\3\6\7\6\u00d5\n\6\f\6\16\6\u00d8\13\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\5\6\u00e0\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\5\7\u00ef\n\7\3\7\3\7\3\7\7\7\u00f4\n\7\f\7\16\7\u00f7\13"+
		"\7\3\7\3\7\7\7\u00fb\n\7\f\7\16\7\u00fe\13\7\3\7\3\7\3\7\3\7\3\7\3\7\5"+
		"\7\u0106\n\7\3\7\3\7\3\7\7\7\u010b\n\7\f\7\16\7\u010e\13\7\3\7\3\7\7\7"+
		"\u0112\n\7\f\7\16\7\u0115\13\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u011d\n\7\3"+
		"\7\2\2\b\2\4\6\b\n\f\2\3\3\2\32\33\u0141\2\16\3\2\2\2\4\33\3\2\2\2\6\u0093"+
		"\3\2\2\2\b\u009c\3\2\2\2\n\u00df\3\2\2\2\f\u011c\3\2\2\2\16\17\5\4\3\2"+
		"\17\20\7\35\2\2\20\25\b\2\1\2\21\23\5\b\5\2\22\24\7\35\2\2\23\22\3\2\2"+
		"\2\23\24\3\2\2\2\24\26\3\2\2\2\25\21\3\2\2\2\26\27\3\2\2\2\27\25\3\2\2"+
		"\2\27\30\3\2\2\2\30\31\3\2\2\2\31\32\b\2\1\2\32\3\3\2\2\2\33\34\7\3\2"+
		"\2\34\35\7\4\2\2\35&\7\5\2\2\36#\5\6\4\2\37 \7\6\2\2 \"\5\6\4\2!\37\3"+
		"\2\2\2\"%\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\'\3\2\2\2%#\3\2\2\2&\36\3\2\2\2"+
		"&\'\3\2\2\2\'(\3\2\2\2()\7\7\2\2)*\7\6\2\2*\5\3\2\2\2+,\7\b\2\2,-\5\n"+
		"\6\2-.\7\t\2\2./\b\4\1\2/\u0094\3\2\2\2\60\61\7\n\2\2\61\62\5\n\6\2\62"+
		"\63\7\t\2\2\63\64\b\4\1\2\64\u0094\3\2\2\2\65\66\7\13\2\2\66\67\5\n\6"+
		"\2\678\7\t\2\289\b\4\1\29\u0094\3\2\2\2:;\7\f\2\2;<\5\n\6\2<=\7\6\2\2"+
		"=>\5\n\6\2>?\7\t\2\2?@\b\4\1\2@\u0094\3\2\2\2AB\7\r\2\2BC\5\n\6\2CD\7"+
		"\6\2\2DE\5\n\6\2EF\7\t\2\2FG\b\4\1\2G\u0094\3\2\2\2HI\7\16\2\2IJ\5\n\6"+
		"\2JK\7\6\2\2KL\5\n\6\2LM\7\t\2\2MN\b\4\1\2N\u0094\3\2\2\2OP\7\17\2\2P"+
		"Q\5\n\6\2QR\7\6\2\2RS\7\32\2\2ST\7\6\2\2TU\5\n\6\2UV\7\t\2\2VW\b\4\1\2"+
		"W\u0094\3\2\2\2XY\7\17\2\2YZ\7\32\2\2Z[\7\6\2\2[\\\5\n\6\2\\]\7\6\2\2"+
		"]^\5\n\6\2^_\7\t\2\2_`\b\4\1\2`\u0094\3\2\2\2ab\7\17\2\2bc\5\n\6\2cd\7"+
		"\6\2\2de\5\n\6\2ef\7\6\2\2fg\5\n\6\2gh\7\t\2\2hi\b\4\1\2i\u0094\3\2\2"+
		"\2jk\7\20\2\2kl\5\n\6\2lm\7\6\2\2mn\5\n\6\2no\7\6\2\2op\5\n\6\2pq\7\t"+
		"\2\2qr\b\4\1\2r\u0094\3\2\2\2st\7\32\2\2tu\7\21\2\2uv\7\32\2\2vw\3\2\2"+
		"\2w\u0094\b\4\1\2xy\7\32\2\2yz\7\21\2\2z\u0080\5\f\7\2{|\5\f\7\2|}\7\21"+
		"\2\2}~\7\32\2\2~\u0080\3\2\2\2\177x\3\2\2\2\177{\3\2\2\2\u0080\u0081\3"+
		"\2\2\2\u0081\u0082\b\4\1\2\u0082\u0094\3\2\2\2\u0083\u0084\7\32\2\2\u0084"+
		"\u0085\7\21\2\2\u0085\u008b\5\n\6\2\u0086\u0087\5\n\6\2\u0087\u0088\7"+
		"\21\2\2\u0088\u0089\7\32\2\2\u0089\u008b\3\2\2\2\u008a\u0083\3\2\2\2\u008a"+
		"\u0086\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\b\4\1\2\u008d\u0094\3\2"+
		"\2\2\u008e\u008f\5\n\6\2\u008f\u0090\7\22\2\2\u0090\u0091\7\32\2\2\u0091"+
		"\u0092\b\4\1\2\u0092\u0094\3\2\2\2\u0093+\3\2\2\2\u0093\60\3\2\2\2\u0093"+
		"\65\3\2\2\2\u0093:\3\2\2\2\u0093A\3\2\2\2\u0093H\3\2\2\2\u0093O\3\2\2"+
		"\2\u0093X\3\2\2\2\u0093a\3\2\2\2\u0093j\3\2\2\2\u0093s\3\2\2\2\u0093\177"+
		"\3\2\2\2\u0093\u008a\3\2\2\2\u0093\u008e\3\2\2\2\u0094\7\3\2\2\2\u0095"+
		"\u0096\7\32\2\2\u0096\u0097\b\5\1\2\u0097\u0098\7\4\2\2\u0098\u0099\5"+
		"\n\6\2\u0099\u009a\b\5\1\2\u009a\u009b\7\6\2\2\u009b\u009d\3\2\2\2\u009c"+
		"\u0095\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2"+
		"\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a1\b\5\1\2\u00a1\t\3\2\2\2\u00a2\u00a3"+
		"\7\33\2\2\u00a3\u00e0\b\6\1\2\u00a4\u00a5\7\32\2\2\u00a5\u00e0\b\6\1\2"+
		"\u00a6\u00a7\7\23\2\2\u00a7\u00a8\7\24\2\2\u00a8\u00a9\t\2\2\2\u00a9\u00aa"+
		"\7\6\2\2\u00aa\u00ab\t\2\2\2\u00ab\u00ac\7\t\2\2\u00ac\u00e0\b\6\1\2\u00ad"+
		"\u00ae\7\25\2\2\u00ae\u00b8\b\6\1\2\u00af\u00b0\7\6\2\2\u00b0\u00b2\b"+
		"\6\1\2\u00b1\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3"+
		"\u00b4\5\n\6\2\u00b4\u00b5\b\6\1\2\u00b5\u00b7\3\2\2\2\u00b6\u00b1\3\2"+
		"\2\2\u00b7\u00ba\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9"+
		"\u00bf\3\2\2\2\u00ba\u00b8\3\2\2\2\u00bb\u00bc\7\26\2\2\u00bc\u00be\7"+
		"\32\2\2\u00bd\u00bb\3\2\2\2\u00be\u00c1\3\2\2\2\u00bf\u00bd\3\2\2\2\u00bf"+
		"\u00c0\3\2\2\2\u00c0\u00c2\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c2\u00c3\7\27"+
		"\2\2\u00c3\u00e0\b\6\1\2\u00c4\u00c5\7\5\2\2\u00c5\u00cf\b\6\1\2\u00c6"+
		"\u00c7\7\6\2\2\u00c7\u00c9\b\6\1\2\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3\2"+
		"\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cb\5\n\6\2\u00cb\u00cc\b\6\1\2\u00cc"+
		"\u00ce\3\2\2\2\u00cd\u00c8\3\2\2\2\u00ce\u00d1\3\2\2\2\u00cf\u00cd\3\2"+
		"\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d6\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d2"+
		"\u00d3\7\30\2\2\u00d3\u00d5\5\n\6\2\u00d4\u00d2\3\2\2\2\u00d5\u00d8\3"+
		"\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d9\3\2\2\2\u00d8"+
		"\u00d6\3\2\2\2\u00d9\u00da\7\7\2\2\u00da\u00e0\b\6\1\2\u00db\u00dc\7\31"+
		"\2\2\u00dc\u00dd\5\n\6\2\u00dd\u00de\b\6\1\2\u00de\u00e0\3\2\2\2\u00df"+
		"\u00a2\3\2\2\2\u00df\u00a4\3\2\2\2\u00df\u00a6\3\2\2\2\u00df\u00ad\3\2"+
		"\2\2\u00df\u00c4\3\2\2\2\u00df\u00db\3\2\2\2\u00e0\13\3\2\2\2\u00e1\u00e2"+
		"\7\33\2\2\u00e2\u011d\b\7\1\2\u00e3\u00e4\7\23\2\2\u00e4\u00e5\7\24\2"+
		"\2\u00e5\u00e6\t\2\2\2\u00e6\u00e7\7\6\2\2\u00e7\u00e8\t\2\2\2\u00e8\u00e9"+
		"\7\t\2\2\u00e9\u011d\b\7\1\2\u00ea\u00eb\7\25\2\2\u00eb\u00f5\b\7\1\2"+
		"\u00ec\u00ed\7\6\2\2\u00ed\u00ef\b\7\1\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef"+
		"\3\2\2\2\u00ef\u00f0\3\2\2\2\u00f0\u00f1\5\f\7\2\u00f1\u00f2\b\7\1\2\u00f2"+
		"\u00f4\3\2\2\2\u00f3\u00ee\3\2\2\2\u00f4\u00f7\3\2\2\2\u00f5\u00f3\3\2"+
		"\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00fc\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f8"+
		"\u00f9\7\26\2\2\u00f9\u00fb\7\32\2\2\u00fa\u00f8\3\2\2\2\u00fb\u00fe\3"+
		"\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00ff\3\2\2\2\u00fe"+
		"\u00fc\3\2\2\2\u00ff\u0100\7\27\2\2\u0100\u011d\b\7\1\2\u0101\u0102\7"+
		"\5\2\2\u0102\u010c\b\7\1\2\u0103\u0104\7\6\2\2\u0104\u0106\b\7\1\2\u0105"+
		"\u0103\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u0108\5\f"+
		"\7\2\u0108\u0109\b\7\1\2\u0109\u010b\3\2\2\2\u010a\u0105\3\2\2\2\u010b"+
		"\u010e\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u0113\3\2"+
		"\2\2\u010e\u010c\3\2\2\2\u010f\u0110\7\30\2\2\u0110\u0112\5\f\7\2\u0111"+
		"\u010f\3\2\2\2\u0112\u0115\3\2\2\2\u0113\u0111\3\2\2\2\u0113\u0114\3\2"+
		"\2\2\u0114\u0116\3\2\2\2\u0115\u0113\3\2\2\2\u0116\u0117\7\7\2\2\u0117"+
		"\u011d\b\7\1\2\u0118\u0119\7\31\2\2\u0119\u011a\5\f\7\2\u011a\u011b\b"+
		"\7\1\2\u011b\u011d\3\2\2\2\u011c\u00e1\3\2\2\2\u011c\u00e3\3\2\2\2\u011c"+
		"\u00ea\3\2\2\2\u011c\u0101\3\2\2\2\u011c\u0118\3\2\2\2\u011d\r\3\2\2\2"+
		"\30\23\27#&\177\u008a\u0093\u009e\u00b1\u00b8\u00bf\u00c8\u00cf\u00d6"+
		"\u00df\u00ee\u00f5\u00fc\u0105\u010c\u0113\u011c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}