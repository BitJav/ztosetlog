package translationEngine.slogToZ;

import net.sourceforge.czt.z.ast.AxPara;


/**
 * Interface that abstracts a Z Scheme.
 * @author Pablo Rodriguez Monetti
 */
interface Scheme extends AxPara{

	public void setMyAxPara(AxPara axPara);
	public AxPara getMyAxPara();
	public AxPara clone();

}


