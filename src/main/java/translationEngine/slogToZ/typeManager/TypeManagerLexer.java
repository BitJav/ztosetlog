// Generated from TypeManager.g4 by ANTLR 4.5.3

package translationEngine.slogToZ.typeManager;
import javax.swing.tree.DefaultMutableTreeNode;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TypeManagerLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, INT=6, NAT=7, BINOP=8, NAME=9, 
		UNOP=10, NUM=11, WS=12;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "INT", "NAT", "BINOP", "NAME", 
		"UNOP", "NUM", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'\\cross'", "'('", "')'", "'\\upto'", "'_{1}'", "'\\num'", "'\\nat'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "INT", "NAT", "BINOP", "NAME", "UNOP", 
		"NUM", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}



	    //hola
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();
		
		public DefaultMutableTreeNode getRoot() {
			return root; //fsdffdsf
		}
		
		public static String printTree(DefaultMutableTreeNode tree){
			if (tree.isLeaf()) 
				return (String) tree.getUserObject();
			else if (tree.getChildCount() == 1)
				if ( ((String) tree.getUserObject()).equals("()"))
					return "(" + printTree((DefaultMutableTreeNode) tree.getChildAt(0)) + ")";
				else
					return (String) tree.getUserObject() + printTree((DefaultMutableTreeNode) tree.getChildAt(0));
			else if (tree.getChildCount() == 2)
				return printTree((DefaultMutableTreeNode) tree.getChildAt(0)) + ((String) tree.getUserObject()) + printTree((DefaultMutableTreeNode) tree.getChildAt(1));
			else {//tiene varios hijos, es un CROSS!
				String returnString = printTree((DefaultMutableTreeNode) tree.getChildAt(0));
				int i = 1;
				while (i < tree.getChildCount()) {
					returnString = returnString.concat("\\cross");
					returnString = returnString.concat(printTree((DefaultMutableTreeNode) tree.getChildAt(i)));
					i++;
				}
				return returnString;
			}
		}


	public TypeManagerLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "TypeManager.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 11:
			WS_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\16\u008c\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b"+
		"\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\5\tN\n\t\3\n\3\n\3\n\3\n\3\n\6\nU\n\n\r\n\16\nV\3\n\7\n"+
		"Z\n\n\f\n\16\n]\13\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\177\n\13\3\f\6\f\u0082\n"+
		"\f\r\f\16\f\u0083\3\r\6\r\u0087\n\r\r\r\16\r\u0088\3\r\3\r\2\2\16\3\3"+
		"\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\3\2\4\4\2C\\c|\5"+
		"\2\13\f\17\17\"\"\u0098\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2"+
		"\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25"+
		"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\3\33\3\2\2\2\5\"\3\2\2\2\7$\3\2\2\2"+
		"\t&\3\2\2\2\13,\3\2\2\2\r\61\3\2\2\2\17\66\3\2\2\2\21M\3\2\2\2\23T\3\2"+
		"\2\2\25~\3\2\2\2\27\u0081\3\2\2\2\31\u0086\3\2\2\2\33\34\7^\2\2\34\35"+
		"\7e\2\2\35\36\7t\2\2\36\37\7q\2\2\37 \7u\2\2 !\7u\2\2!\4\3\2\2\2\"#\7"+
		"*\2\2#\6\3\2\2\2$%\7+\2\2%\b\3\2\2\2&\'\7^\2\2\'(\7w\2\2()\7r\2\2)*\7"+
		"v\2\2*+\7q\2\2+\n\3\2\2\2,-\7a\2\2-.\7}\2\2./\7\63\2\2/\60\7\177\2\2\60"+
		"\f\3\2\2\2\61\62\7^\2\2\62\63\7p\2\2\63\64\7w\2\2\64\65\7o\2\2\65\16\3"+
		"\2\2\2\66\67\7^\2\2\678\7p\2\289\7c\2\29:\7v\2\2:\20\3\2\2\2;<\7^\2\2"+
		"<=\7t\2\2=>\7g\2\2>N\7n\2\2?@\7^\2\2@A\7r\2\2AB\7h\2\2BC\7w\2\2CN\7p\2"+
		"\2DE\7^\2\2EF\7h\2\2FG\7h\2\2GH\7w\2\2HN\7p\2\2IJ\7^\2\2JK\7h\2\2KL\7"+
		"w\2\2LN\7p\2\2M;\3\2\2\2M?\3\2\2\2MD\3\2\2\2MI\3\2\2\2N\22\3\2\2\2OU\t"+
		"\2\2\2PQ\7^\2\2QR\7a\2\2RU\7\"\2\2SU\7A\2\2TO\3\2\2\2TP\3\2\2\2TS\3\2"+
		"\2\2UV\3\2\2\2VT\3\2\2\2VW\3\2\2\2W[\3\2\2\2XZ\4\62;\2YX\3\2\2\2Z]\3\2"+
		"\2\2[Y\3\2\2\2[\\\3\2\2\2\\\24\3\2\2\2][\3\2\2\2^_\7^\2\2_`\7r\2\2`a\7"+
		"q\2\2ab\7y\2\2bc\7g\2\2c\177\7t\2\2de\7u\2\2ef\7g\2\2fg\7s\2\2gh\7a\2"+
		"\2hi\7}\2\2ij\7\63\2\2j\177\7\177\2\2kl\7u\2\2lm\7g\2\2mn\7s\2\2no\7a"+
		"\2\2op\7}\2\2pq\7\63\2\2qr\7\177\2\2r\177\7\u0080\2\2st\7^\2\2tu\7u\2"+
		"\2uv\7g\2\2v\177\7s\2\2wx\7^\2\2xy\7h\2\2yz\7k\2\2z{\7p\2\2{|\7u\2\2|"+
		"}\7g\2\2}\177\7v\2\2~^\3\2\2\2~d\3\2\2\2~k\3\2\2\2~s\3\2\2\2~w\3\2\2\2"+
		"\177\26\3\2\2\2\u0080\u0082\4\62;\2\u0081\u0080\3\2\2\2\u0082\u0083\3"+
		"\2\2\2\u0083\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084\30\3\2\2\2\u0085"+
		"\u0087\t\3\2\2\u0086\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088\u0086\3\2"+
		"\2\2\u0088\u0089\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008b\b\r\2\2\u008b"+
		"\32\3\2\2\2\n\2MTV[~\u0083\u0088\3\3\r\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}