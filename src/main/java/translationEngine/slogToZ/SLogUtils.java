package translationEngine.slogToZ;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import translationEngine.slogToZ.typeManager.TypeManagerLexer;
import translationEngine.slogToZ.typeManager.TypeManagerParser;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Map;

public final class SLogUtils {

	private static Map<String,String> tipos;
	private static Map<String, String> zNames;
	private static Map<String, StringBuilder> slvars;
	
	static void setLogToLatexCharsReplacerInit(Map<String,String> t,Map<String, StringBuilder> slv,Map<String, String> zN){
		tipos = t;
		slvars = slv;
		zNames = zN;
	}
	//cambia los caracteres de setlog [] por langlerangle, etc...
	static String setLogToLatexCharsReplacer(DefaultMutableTreeNode nodo,String exprS) throws Exception{
		
		if (exprS.equals("ValueNotAssigned") ){
			return exprS;
		}

		if (tipos.containsKey(exprS) && tipos.get(exprS).startsWith("BasicConstant"))
			return exprS;

		if(SLogUtils.esSLVariableSimple(exprS) && slvars.get(exprS) != null && slvars.get(exprS).toString().equals("ValueNotAssigned")){
			if (SLogUtils.esSLVariableSimple_(exprS))
				exprS = SLogUtils.sacar_(exprS);

			return zNames.get(exprS)!=null?zNames.get(exprS):exprS;
		}

		ExprIterator expr = new ExprIterator(exprS);

		if(exprS.startsWith("int(")){
			String aux[] = exprS.substring(4,exprS.length()-1).split(",");
			return setLogToLatexCharsReplacer(new DefaultMutableTreeNode("\\int"), aux[0]) + " \\upto " + setLogToLatexCharsReplacer(new DefaultMutableTreeNode("\\int"), aux[1]) ;
		}
		
		String ct = nodo.toString();

		if (ct.equals("()")) 
			return "(" + setLogToLatexCharsReplacer((DefaultMutableTreeNode) nodo.getChildAt(0),exprS) + ")";

		if (ct.equals("\\pfun")||ct.equals("\\fun")||ct.equals("\\ffun")||ct.equals("\\rel")){
			StringBuilder salida = new StringBuilder();
			String coma = ct.equals("\\rel")?",":"\\mapsto ";
			ExprIterator exprAux;
			while(expr.hasNext()){
				exprAux = new ExprIterator(expr.next());
				salida.append("," + "(").append(setLogToLatexCharsReplacer((DefaultMutableTreeNode) nodo.getChildAt(0),
						exprAux.next())).append(coma).append(setLogToLatexCharsReplacer((DefaultMutableTreeNode)
						nodo.getChildAt(1), exprAux.next())).append(")");
			}
			if (!"".equals(salida.toString()))
				return "\\{" + salida.substring(1) + "\\}";
			return "\\emptyset";
		}

		if (ct.equals("\\cross")){
			StringBuilder salida = new StringBuilder();
			String coma = nodo.getChildCount()>2?",":" \\mapsto ";
			int i = 0;
			while(expr.hasNext()){
				salida.append(coma).append(setLogToLatexCharsReplacer((DefaultMutableTreeNode)
						nodo.getChildAt(i), expr.next()));
				i++;
			}
			return "(" + salida.substring(coma.length()) + ")";
		}

		if (ct.equals("\\power")){
			StringBuilder salida = new StringBuilder();
			while(expr.hasNext()){
				salida.append(",").append(setLogToLatexCharsReplacer((DefaultMutableTreeNode) nodo.getChildAt(0), expr.next()));
			}
			if (!"".equals(salida.toString()))
				return "\\{" + salida.substring(1) + "\\}";
			return "\\emptyset";
		}

		if (ct.equals("\\seq")){
			StringBuilder salida = new StringBuilder();
			while(expr.hasNext())
				salida.append(",").append(setLogToLatexCharsReplacer((DefaultMutableTreeNode) nodo.getChildAt(0), expr.next()));

			if (!"".equals(salida.toString()))
				return "\\langle " + salida.substring(1) + "\\rangle";
			return "\\langle\\rangle";
		}


		String tipocompleto = tipos.get(ct);

		if (tipocompleto !=null){

			if (tipocompleto.startsWith("SchemaType")){
				ExprIterator tiposDecl = SLogUtils.schemaToTypeExprIterator(tipocompleto);
				ExprIterator varsDecl = SLogUtils.schemaToVarExprIterator(tipocompleto);
				String c,v;
				StringBuilder salida = new StringBuilder();
				while(tiposDecl.hasNext()){
					c = tiposDecl.next();
					v = varsDecl.next();
					salida.append(",").append(v).append("==").append(setLogToLatexCharsReplacer(SLogUtils.toTreeNorm(c), expr.next()));
				}
				if (!"".equals(salida.toString()))
					return "\\lblot " + salida.substring(1) + " \\rblot";
				return "\\lblot\\rblot";
			}


			if (tipocompleto.startsWith("EnumerationType"))
				return zNames.get(exprS);



			if (tipocompleto.startsWith("BasicType")){
				//String salida = zNames.get(exprS);
				//salida = ct.toLowerCase() + (salida!=null?salida:getNumber());
				//String salida = ct.toLowerCase() + getNumber();
				//salida = salida.replace("?","");
				return exprS;
			}
		}

		return exprS.replace("_","");
	}

	//por que se hace varias veces
	static DefaultMutableTreeNode toTree(String tipo){
		ANTLRInputStream input = new ANTLRInputStream(tipo);
		TypeManagerLexer lexer = new TypeManagerLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		TypeManagerParser TMP = new TypeManagerParser(tokens);
		TMP.typeManage();
		return TMP.getRoot();
	}

	static DefaultMutableTreeNode toTreeNorm(String tipo){
		ANTLRInputStream input = new ANTLRInputStream(tipo);
		TypeManagerLexer lexer = new TypeManagerLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		TypeManagerParser TMP = new TypeManagerParser(tokens);
		TMP.typeManageNorm();
		return TMP.getRoot();
	}

	static boolean esSLVariableSimple(String expr){
		if (expr==null || expr.equals(""))
			return false;
		char c = expr.charAt(0);
		return (expr.startsWith("_")|| Character.isUpperCase(c));
	}

	static boolean esSLVariableSimple_(String expr){
		if (expr==null || expr.equals(""))
			return false;
		char c = expr.charAt(0);
		return expr.startsWith("_");
	}

	static String sacar_(String expr){
		return expr.substring(1);
	}







	static boolean esSLCteSimple(String expr) {
		if (expr == null || expr.equals(""))
			return false;
		char c = expr.charAt(0);
		return !expr.startsWith("int(") && (Character.isLowerCase(c) || Character.isDigit(c) || c == '-');
	}

//	public static boolean esSLNum(String expr){
//		if (expr==null || expr.equals(""))
//			return false;
//		char c = expr.charAt(0);
//		return (Character.isDigit(c) || c == '-');
//	}

	static ExprIterator schemaToVarExprIterator(String tipoCompleto){
		// ej SchemaType:Estado:[var1:\num,var2:E] -> {var1,var2}
		// "SchemaType:".length() = 11 + :.lingth()
		//tipoCompleto = tipoCompleto.substring(12+nomTipo.length());
		tipoCompleto = tipoCompleto.substring(tipoCompleto.indexOf(':')+1);
		tipoCompleto = tipoCompleto.substring(tipoCompleto.indexOf(':')+1);
		ExprIterator expr = new ExprIterator(tipoCompleto);
		String elem,aux[];
		StringBuilder salida = new StringBuilder();
		while(expr.hasNext()){
			elem = expr.next();
			aux = elem.split(":");
			salida.append(",").append(aux[0]);
			//System.out.println(elem);
		}
		String s = "{" + salida.substring(1) + "}";
		return new ExprIterator(s);
	}

	static ExprIterator schemaToTypeExprIterator(String tipoCompleto){
		// ej SchemaType:Estado:[var1:\num,var2:E]-> {\num,E}
		// "SchemaType:".length() = 11 + :.lingth()
		//tipoCompleto = tipoCompleto.substring(12+nomTipo.length());
		tipoCompleto = tipoCompleto.substring(tipoCompleto.indexOf(':')+1);
		tipoCompleto = tipoCompleto.substring(tipoCompleto.indexOf(':')+1);
		ExprIterator expr = new ExprIterator(tipoCompleto);
		String elem,aux[];
		StringBuilder salida = new StringBuilder();
		while(expr.hasNext()){
			elem = expr.next();
			aux = elem.split(":");
			salida.append(",").append(aux[1]);
			//System.out.println(elem);
		}
		String s = "{" + salida.substring(1) + "}";
		return new ExprIterator(s);
	}
	
}
