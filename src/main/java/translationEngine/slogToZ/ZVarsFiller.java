package translationEngine.slogToZ;

import java.util.Iterator;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

final class ZVarsFiller {

	private String setlogOutput;
	private Map<String,String> zVars;
	private Map<String,String> tipos;
	private Map<String, String> zNames;
	private Map<String, StringBuilder> slvars;

	private void setLogToLatexCharsReplacer() throws Exception{
		SLogUtils.setLogToLatexCharsReplacerInit(tipos, slvars, zNames);
		Iterator<String> it = zVars.keySet().iterator();
		String var,tipo,expr;
		String varn;
		while (it.hasNext()) {  
			var = it.next();
			tipo = tipos.get(var);
			expr = zVars.get(var);
			varn = SLogUtils.setLogToLatexCharsReplacer(SLogUtils.toTree(tipo),expr);
			varn = varn.replace("-", "\\negate ");
			zVars.put(var,varn);
		}
	}
	private void llenarZVars(ConstantCreator cc){
		Map<String, String> memory = zNames;
		Iterator<String> iterator = zVars.keySet().iterator();  
		String key,valor;
		while (iterator.hasNext()) {  
			key = iterator.next();
			valor = zVars.get(key);
			if (valor == null || valor.equals("")){
				String tipo = tipos.get(key);
				valor =  cc.getCte(memory.get(key), SLogUtils.toTreeNorm(tipo));
				zVars.put(key, valor);
			}  
		}
	}

	//traduccion de SLog a Z
	void generar() throws Exception{

		ANTLRInputStream input = new ANTLRInputStream(setlogOutput);
		SLog2ZLexer lexer2 = new SLog2ZLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer2);
		SLog2ZParser SL2ZP = new SLog2ZParser(tokens);
		SL2ZP.loadTablas(zVars,tipos, zNames);
		SL2ZP.lineas();
		this.slvars = SL2ZP.getSlvars();
		llenarZVars(SL2ZP.getCC());

		setLogToLatexCharsReplacer();
	}




	ZVarsFiller(Map<String,String> zVars, Map<String,String> tipos, Map<String,String> zNames, String setlogOutput){
		this.zVars = zVars;
		this.tipos = tipos;
		this.zNames = zNames;
		this.setlogOutput = setlogOutput;
	}



}
