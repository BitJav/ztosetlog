package czt;

import czt.visitors.CZTCloner;
import czt.visitors.PrimedVarExtractor;
import net.sourceforge.czt.animation.eval.ZLive;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.parser.z.ParseUtils;
import net.sourceforge.czt.session.CommandException;
import net.sourceforge.czt.session.StringSource;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;
import net.sourceforge.czt.z.visitor.*;
import translationEngine.slogToZ.ToZState;

import java.io.IOException;
import java.util.*;

/**
 * Created by joaquin on 02/04/17 z2setlog.
 */
public class PrimedVarAdder {

    private AxPara axPara;
    private Map<String,String> vars;
    private List<String> primedVarUsed;

    public PrimedVarAdder(AxPara axPara, Map<String,String> vars){
        this.vars = vars;
        this.axPara = axPara;

        primedVarUsed = axPara.accept(new PrimedVarExtractor());

    }

    public AxPara buildNewAxPara(){

        ZLive zLive = UniqueZLive.getInstance();
        RefExpr svarPrimed;
        Expr svar;
        String value;
        Map<RefExpr, Expr> map = new HashMap<RefExpr, Expr>();
        for (String key: vars.keySet()){
            value = vars.get(key);
            if (!primedVarUsed.contains(value)){
                try {
                    svarPrimed =(RefExpr) ParseUtils.parseExpr(new StringSource(value),
                            zLive.getCurrentSection(), zLive.getSectionManager());
                    svar = ParseUtils.parseExpr(new StringSource(key), zLive.getCurrentSection(),
                            zLive.getSectionManager());
                    map.put(svarPrimed, svar);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (CommandException e) {
                    e.printStackTrace();
                }
            }
        }


        return fillAxPara(axPara, map);
    }

    private AxPara fillAxPara(AxPara axPara, Map<RefExpr, Expr> varExprMap){
        AxPara newAxPara = (AxPara) axPara.accept(new CZTCloner());

        ZSchText zSchText = axPara.getZSchText();
        ZDeclList zDeclList = zSchText.getZDeclList();
        ConstDecl constDecl = (ConstDecl) zDeclList.get(0);
        SchExpr schExpr = (SchExpr) constDecl.getExpr();
        ZSchText zSchText1 = schExpr.getZSchText();

        Pred pred = SpecUtils.andPreds(zSchText1.getPred(),SpecUtils.createAndPred(varExprMap));

        SpecUtils.setAxParaPred(newAxPara, pred);

        return  newAxPara;

    }
}
