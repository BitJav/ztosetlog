package czt;

import net.sourceforge.czt.animation.eval.ZLive;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.VarDeclImpl;
import net.sourceforge.czt.z.impl.ZFactoryImpl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;


/**
 * Created by jmesuro on 04/05/16.
 */
public final class SpecUtils {


    public static String termToLatex(Term term) {
        StringWriter out = new StringWriter();
        ZLive zLive = UniqueZLive.getInstance();
        zLive.printTerm(new PrintWriter(out), term, zLive.getMarkup());
        return out.toString();
    }

    public static DeclList getAxParaListOfDecl(AxPara axPara) {
        DeclList declList = null;
        SchExpr schExpr = getAxParaSchExpr(axPara);
        if (schExpr != null) {
            ZSchText zSchText = schExpr.getZSchText();
            if (zSchText != null) {
                declList = zSchText.getDeclList();
            }
        }
        return declList;
    }









    public static SchExpr getAxParaSchExpr(AxPara axPara) {
        SchExpr schExpr = null;
        ConstDecl constDecl = getConstDecl(axPara);
        if (constDecl != null) {
            Expr expr = constDecl.getExpr();
            if (expr instanceof SchExpr) {
                schExpr = (SchExpr) expr;
            }
        }
        return schExpr;
    }
    private static ConstDecl getConstDecl(AxPara axPara) {
        ConstDecl constDecl = null;
        ZSchText zSchText = axPara.getZSchText();
        DeclList declList = zSchText.getDeclList();
        if (declList instanceof ZDeclList) {
            int declListSize = ((ZDeclList) declList).size();
            for (int j = 0; j < declListSize; j++) {
                Decl decl = ((ZDeclList) declList).get(j);
                if (decl instanceof ConstDecl) {
                    constDecl = (ConstDecl) decl;
                }
            }
        }
        return constDecl;
    }



    public static ZParaList getZParaList(Spec spec){
        ZParaList zParaList = null;
        for (Sect sect : spec.getSect()) {
            if (sect instanceof ZSect) {
                ParaList paraList = ((ZSect) sect).getParaList();
                if (paraList instanceof ZParaList) {
                    zParaList = (ZParaList) paraList;
                }
            }
        }
        return zParaList;
    }

    public static void setAxParaListOfDecl(AxPara axPara, DeclList declList) {
        SchExpr schExpr = getAxParaSchExpr(axPara);
        if (schExpr != null) {
            ZSchText zSchText = schExpr.getZSchText();
            if (zSchText != null) {
                zSchText.setDeclList(declList);
            }
        }
    }

    public static void setAxParaName(AxPara axPara, String newName) {
        ConstDecl constDecl = getConstDecl(axPara);
        if (constDecl != null) {
            Name name = constDecl.getName();
            if (name instanceof ZName) {
                ((ZName) name).setWord(newName);
            }
        }
    }





    public static void setAxParaPred(AxPara axPara, Pred pred) {
        ZSchText schText = getAxParaSchExpr(axPara).getZSchText();
        schText.setPred(pred);
    }



    public static Pred createAndPred(Map<RefExpr, Expr> varExprMap) {
        ZFactory zFactory = new ZFactoryImpl();
        Set<Map.Entry<RefExpr, Expr>> set = varExprMap.entrySet();
        Iterator<Map.Entry<RefExpr, Expr>> iterator = set.iterator();
        Pred pred = null;
        while (iterator.hasNext()) {
            Map.Entry<RefExpr, Expr> mapEntry = iterator.next();
            RefExpr refExpr = mapEntry.getKey();
            Expr expr = mapEntry.getValue();

            //We create the SetExpr that contains the right expr (esto es por que asi lo necesita createMemPred)
            ZExprList setZExprList = zFactory.createZExprList();
            setZExprList.add(0, expr);
            SetExpr setExpr = zFactory.createSetExpr(setZExprList);

            //We create the MemPred that the method will return
            ZExprList memPredExprList = zFactory.createZExprList();
            memPredExprList.add(0, refExpr);
            memPredExprList.add(1, setExpr);
            MemPred memPred = zFactory.createMemPred(memPredExprList, true);
            if (pred != null) {
                pred = andPreds(pred, memPred);
            } else {
                pred = memPred;
            }
        }
        return pred;

    }

    public static String getAxParaName(AxPara axPara) {
        String axParaName = "";
        ConstDecl constDecl = getConstDecl(axPara);
        if (constDecl != null) {
            Name name = constDecl.getName();
            if (name instanceof ZName) {
                axParaName = (((ZName) name).getWord());
            }
        }
        return axParaName;
    }









    static Pred andPreds(Pred pred1, Pred pred2) {
        if (pred1 == null) {
            return pred2;
        } else if (pred2 == null) {
            return pred1;
        }
        // If pred1 and pred2 are not equal to null
        AndPred newPred = (new ZFactoryImpl()).createAndPred();
        newPred.setLeftPred(pred1);
        newPred.setRightPred(pred2);
        newPred.setAnd(And.NL);
        return newPred;
    }

     public static AxPara axParaSearch(String name, ZParaList zParaList) {
        AxPara axPara = null;
        for (Para para : zParaList)
            if (para instanceof AxPara && getAxParaName((AxPara) para).equals(name))
                axPara = ((AxPara) para);

        return axPara;
    }

    // Devuelve la lista de variables input de un esquema de operación
    // Las que tienen "?"
    public static Set<String> getVarInputs(AxPara axPara){
        ZSchText zSchText = axPara.getZSchText();
        ZDeclList zDeclList = zSchText.getZDeclList();
        ConstDecl constDecl = (ConstDecl) zDeclList.get(0);
        SchExpr schExpr = (SchExpr) constDecl.getExpr();
        ZSchText zSchText1 = schExpr.getZSchText();
        ZDeclList zDeclList1 = zSchText1.getZDeclList();
        Set<String> set = new HashSet<String>();
        for (Decl decl : zDeclList1){
            if (decl instanceof VarDeclImpl){
                VarDeclImpl varDecl = (VarDeclImpl) decl;
                String name = varDecl.getName().toString();
                if (name.endsWith("?"))
                    set.add(name);
            }
        }
        return set;
    }


}