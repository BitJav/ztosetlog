package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.Expr;
import net.sourceforge.czt.z.ast.Name;
import net.sourceforge.czt.z.ast.RefExpr;
import net.sourceforge.czt.z.ast.ZExprList;
import net.sourceforge.czt.z.util.Factory;
import net.sourceforge.czt.z.visitor.RefExprVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by joaquin on 04/07/16.
 */
public final class RefExprReplacer implements
        TermVisitor<Term>,
        RefExprVisitor<Term>
    {
        private Map<String,Expr> constDecls;

        public RefExprReplacer(Map<String,Expr> constDecls)
        {
            this.constDecls = constDecls;
        }
        public Term visitTerm(Term term)
        {
            Object[] args = term.getChildren();
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof Term) {
                    args[i] = ((Term) args[i]).accept(this);
                }
            }
            return term.create(args);
        }

        public Term visitRefExpr(RefExpr refExpr) {
            String ref = refExpr.getZName().toString();
            Expr expr = refExpr;

            if (constDecls.containsKey(ref))
                expr = (Expr) constDecls.get(ref).accept(this);
            else {
                List<Expr> lexpr = new ArrayList<Expr>();
                Factory factory = new Factory();
                if (refExpr.getMixfix() && refExpr.getExplicit()){
                    for (Expr expri : refExpr.getZExprList())
                        lexpr.add((Expr) expri.accept(this));
                    Name name = factory.createZName(ref);
                    ZExprList zExprList = factory.createZExprList(lexpr);
                    expr =  factory.createRefExpr(name, zExprList, refExpr.getMixfix(), refExpr.getExplicit());
                }
            }

            return expr;
        }
    }


