package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.GivenPara;
import net.sourceforge.czt.z.ast.Name;
import net.sourceforge.czt.z.visitor.GivenParaVisitor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jmesuro on 07/05/16.
 */
public final class BasicTypeNamesExtractor implements GivenParaVisitor<Set<String>>,
        TermVisitor<Set<String>> {

    public Set<String> visitGivenPara(GivenPara givenPara) {
        Set<String> basicTypeNames = new HashSet<String>();

        for (Name name : givenPara.getZNameList())
            basicTypeNames.add(name.toString());

        return basicTypeNames;
    }

    public Set<String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Set<String> basicTypeNames = new HashSet<String>();

        for (final Object object : array)
            if (object instanceof Term)
                basicTypeNames.addAll(((Term) object).accept(this));

        return basicTypeNames;
    }
}