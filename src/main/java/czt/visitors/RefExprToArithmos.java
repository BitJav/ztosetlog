package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.base.visitor.VisitorUtils;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;
import net.sourceforge.czt.z.visitor.RefExprVisitor;
import rwrules.z.UtilSymbols;

/**
 * An instance of this class allow the replacement of any RefExpr for a reference to 
 * arithmos. This class is based on the Visitor
 * design pattern.
 */
final class RefExprToArithmos
	implements  TermVisitor<Term>, RefExprVisitor<Term>{

	public Term visitTerm(Term term){
		TypeAnn s = term.getAnn(TypeAnn.class);
		if(s!=null)
		{
			Type type = s.getType();
			boolean b;
			if(type instanceof PowerType)
				b = term.getAnns().remove(s);
		}
		return VisitorUtils.visitTerm(this, term, false);
	}

	public Term visitRefExpr(RefExpr refExpr){
		ZFactory zFactory = new ZFactoryImpl();
		ZName zName = zFactory.createZName(UtilSymbols.getSymbol(3), zFactory.createZStrokeList(),"");
		return zFactory.createRefExpr(zName, zFactory.createZExprList(),false,false);
	}

} 
