package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.ApplExpr;
import net.sourceforge.czt.z.ast.Expr;
import net.sourceforge.czt.z.ast.RefExpr;
import net.sourceforge.czt.z.visitor.ApplExprVisitor;
import rwrules.z.UtilSymbols;

import java.util.ArrayList;
import java.util.List;

public final class FuncApplExtractor
        implements TermVisitor<List<Term>>, ApplExprVisitor<List<Term>> {

    private String symbol;

    public FuncApplExtractor(String operator) {
        if (operator.equals("\\cup"))
            symbol = UtilSymbols.unionSymbol();
        else if (operator.equals("\\cap"))
            symbol = UtilSymbols.intersectionSymbol();
    }

    public List<Term> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Term auxTerm;
        List<Term> termList = new ArrayList<Term>();
        for (final Object object : array) {
            if (object instanceof Term) {
                auxTerm = (Term) object;
                termList.addAll(auxTerm.accept(this));
            }
        }
        return termList;
    }

    public List<Term> visitApplExpr(ApplExpr applExpr) {
        List<Term> termList = new ArrayList<Term>();
        Expr leftExpr = applExpr.getLeftExpr();
        if (leftExpr instanceof RefExpr) {
            RefExpr refExpr = (RefExpr) leftExpr;
            String refExprStr = refExpr.getZName().toString();
            if (refExprStr.equals(symbol)) {
                termList.add(applExpr);
                return termList;
            }
        }
        return termList;
    }
}