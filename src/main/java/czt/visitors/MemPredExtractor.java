package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.MemPredVisitor;
import rwrules.z.UtilSymbols;
import java.util.ArrayList;
import java.util.List;


public final class MemPredExtractor
	implements  TermVisitor<List<Term>>, MemPredVisitor<List<Term>>{

	private String operator;
	private String symbol;

	public MemPredExtractor(String operator) {
		this.operator = operator;
		if (operator.equals("\\geq"))
			symbol = UtilSymbols.gecSymbol();
		else if (operator.equals("\\leq"))
			symbol = UtilSymbols.leqSymbol();
		else if (operator.equals("\\neq"))
			symbol = UtilSymbols.neqSymbol();
		else if (operator.equals("<"))
			symbol = UtilSymbols.lessThanSymbol();
		else if (operator.equals(">"))
			symbol = UtilSymbols.greaterThanSymbol();
	}

	public List<Term> visitTerm(Term term) {
		Object[] array = term.getChildren();
		Term auxTerm;
		List<Term> termList = new ArrayList<Term>();
		for (final Object object : array) {
			if (object instanceof Term) {
				auxTerm = (Term) object;
				termList.addAll(auxTerm.accept(this));
			}
		}
		return termList;
	}

	public List<Term> visitMemPred(MemPred memPred) {
		List<Term> termList = new ArrayList<Term>();
		Expr rightExpr = memPred.getRightExpr();
		boolean isEquality = false;
		if (rightExpr instanceof SetExpr) {
			SetExpr auxSet = (SetExpr) rightExpr;
			ZExprList zList = auxSet.getZExprList();
			if (zList.size() == 1)
				isEquality = true;
		}
		if (isEquality) {
			if (operator.equals("="))
				termList.add(memPred);
			return termList;
		} else {
			if (rightExpr instanceof RefExpr) {
				RefExpr refAux = (RefExpr) rightExpr;
				String refExprStr = refAux.getZName().toString();
				if (refExprStr.equals(symbol)) {
					termList.add(memPred);
					return termList;
				}
			}
			return termList;
		}
	}
}