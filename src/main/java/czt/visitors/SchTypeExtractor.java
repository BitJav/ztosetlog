package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joaquin on 03/11/16.
 * Por si algún día quiero emprolijar el hecho de que se use SchType como Cross
 * Entonces debería tener este extractor. como lo hago con FreeType.
 */
public final class SchTypeExtractor implements
                TermVisitor<Map<String, Map<String,String>>>,
                VarDeclVisitor<Map<String,Map<String,String>>>{


    public Map<String, Map<String,String>> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Map<String, Map<String,String>> schType = new HashMap<String, Map<String,String>>();
        for (final Object object : array) {
            if (object instanceof Term) {
                schType.putAll(((Term) object).accept(this));
            }
        }
        return schType;
    }

    private Map<String,String> varDeclNoDeep(VarDecl varDecl){
        Map<String,String> map = new HashMap<String,String>();
        for (Name var : varDecl.getZNameList())
            map.put(var.toString(), SpecUtils.termToLatex(varDecl.getExpr()) );

        return map;
    }

    private Map<String,String> schDeclVars(SchExpr schExpr){
        ZSchText zSchText = schExpr.getZSchText();
        Map<String,String> ivars = new HashMap<String, String>();

        for (Decl decli : zSchText.getZDeclList())
            ivars.putAll( varDeclNoDeep((VarDecl) decli));
        return ivars;
    }

    public Map<String, Map<String,String>> visitVarDecl(VarDecl varDecl) {
        Map<String,Map<String,String>> map = new HashMap<String,Map<String,String>>();

        for (Name var : varDecl.getZNameList()) {
            Expr expr = varDecl.getExpr();
            //map.putAll(expr.accept(this));

            if (expr instanceof SchExpr){
                map.put(var.toString(),schDeclVars((SchExpr) expr));
                map.putAll(expr.accept(this));
            }

        }
        return map;
    }




}
