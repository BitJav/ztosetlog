package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;
import net.sourceforge.czt.z.visitor.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import  czt.SpecUtils;


/**
 * Borra los esquemas de operaciones y los esquemas Decls de la especificacin
 * Además agrega el esquema de la clase de prueba a la cual se le generará el caso.
 * Created by joaquin on 03/02/17 z2setlog.
 */
public class SpecCleaner implements    ZSectVisitor<Term>,
                                        SpecVisitor<Term>,
                                        ZParaListVisitor<Term> {



    private Collection<String> opNamesRep;
    private AxPara axPara;

    public SpecCleaner(AxPara axPara, Spec spec){
        OpNamesExtractor opNamesExtractor = new OpNamesExtractor(spec);
        spec.accept(opNamesExtractor);
        opNamesRep = opNamesExtractor.getOpNames();
        this.axPara = axPara;
    }

    public Term visitSpec(Spec spec) {
        ZFactory zFactory = new ZFactoryImpl();

        List<Sect> sectList = new ArrayList<Sect>();
        for (Sect sect : spec.getSect()){
            sectList.add((Sect) sect.accept(this));
        }

        return zFactory.createSpec(sectList,spec.getVersion());
    }


    public Term visitZSect(ZSect zSect) {
        ZParaList zParaList = null;
        ParaList paraList = zSect.getParaList();
        if (paraList instanceof ZParaList) {
            zParaList = (ZParaList) paraList.accept(this);
        }


        return (new ZFactoryImpl()).createZSect(zSect.getName(),zSect.getParent(),zParaList);
    }

    public Term visitZParaList(ZParaList zParaList) {

        ZFactory zFactory = new ZFactoryImpl();
        List<Para> paraList = new ArrayList<Para>();

        String name;
        //recorre la lista de los AxPara obvia los que son operaciones y los esquemas de declaraciónes
        for (Para para : zParaList){
            if (para instanceof AxPara){
                name = SpecUtils.getAxParaName((AxPara)para);
                if (!opNamesRep.contains(name) && !name.contains("Decls"))
                    paraList.add(para);
            }
            else
                    paraList.add(para);

        }
        paraList.add(this.axPara);

        return zFactory.createZParaList(paraList);
    }



}
