package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.Expr;
import net.sourceforge.czt.z.ast.Name;
import net.sourceforge.czt.z.ast.RefExpr;
import net.sourceforge.czt.z.ast.VarDecl;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joaquin on 14/07/16.
 */
public class SeqExtractor implements
        TermVisitor<Set<String>>,
        VarDeclVisitor<Set<String>>{

    public Set<String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Set<String> seqs = new HashSet<String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                seqs.addAll(((Term) object).accept(this));
            }
        }
        return seqs;
    }


    public Set<String> visitVarDecl(VarDecl varDecl) {
        Expr expr = varDecl.getExpr();
        Set<String> seqs = new HashSet<String>();
        if (expr instanceof RefExpr){
            String zvar;
            RefExpr refExpr = (RefExpr) expr;
            for (Name var : varDecl.getZNameList()) { // x,y : Expr
                zvar = var.toString();
                String name = refExpr.getZName().toString();
                if (refExpr.getMixfix() && (name.equals("seq _ ") || name.equals("seq _ ↘1↖")))
                    seqs.add(zvar);
            }
        }
        return seqs;
    }
}
