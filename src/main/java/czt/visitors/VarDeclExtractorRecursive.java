package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.InclDeclVisitor;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;

import java.util.*;

/**
 * Created by joaquin on 06/10/16 ztosetlog.
 */
public class VarDeclExtractorRecursive implements
        VarDeclVisitor<Map<String,String>>,
        TermVisitor<Map<String,String>>,
        InclDeclVisitor<Map<String,String>>
{

    private Spec spec;

    public VarDeclExtractorRecursive(Spec spec){
        this.spec = spec;
    }

    public Map<String,String> visitVarDecl(VarDecl varDecl) {

        Map<String,String> map = new HashMap<String,String>();
        for (Name var : varDecl.getZNameList()) {
            Expr expr = varDecl.getExpr();
            map.putAll(expr.accept(this));
            map.put(var.toString(), SpecUtils.termToLatex(expr));
        }
        return map;
    }

    public Map<String,String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Term auxTerm;
        Map<String,String> map = new HashMap<String,String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                auxTerm = (Term) object;
                map.putAll(auxTerm.accept(this));
            }
        }
        return map;
    }



    public Map<String, String> visitInclDecl(InclDecl inclDecl) {
        // (RefExpr)inclDecl.getExpr()).getZName()
        Expr expr = inclDecl.getExpr();
        Map<String,Map<String, String>> m = null;
        if (expr instanceof RefExpr){
            String refExprName = ((RefExpr) expr).getZName().getWord();
            if (((RefExpr) expr).getZName().getId().equals("deltaxi"))
                 refExprName = refExprName.substring(1);
            Collection<String> collection = new ArrayList<String>();
            collection.add(refExprName);
            AxPara axPara =  spec.accept(new AxParaExtractor(collection)).get(refExprName);
            VarDeclExtractorByOps varDeclExtractorByOps = new VarDeclExtractorByOps(spec);
            axPara.accept(varDeclExtractorByOps);
            m = varDeclExtractorByOps.getOps();
        }

        //if ()
        // (RefExpr)inclDecl.getExpr()).getZName()
//        Object[] array = inclDecl.getChildren();
//        Term auxTerm;
//        Map<String,String> map = new HashMap<String,String>();
//        for (final Object object : array) {
//            if (object instanceof Term) {
//                auxTerm = (Term) object;
//                map.putAll(auxTerm.accept(this));
//            }

        return m.values().iterator().next();
    }
}