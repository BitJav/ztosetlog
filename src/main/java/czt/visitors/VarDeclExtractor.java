package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;

import java.util.*;

/**
 * Created by joaquin on 06/10/16 ztosetlog.
 */
public class VarDeclExtractor implements
        VarDeclVisitor<Map<String,String>>,
        TermVisitor<Map<String,String>>
{

    public Map<String,String> visitVarDecl(VarDecl varDecl) {

        Map<String,String> map = new HashMap<String,String>();
        for (Name var : varDecl.getZNameList()) {
            Expr expr = varDecl.getExpr();
            map.putAll(expr.accept(this));
            map.put(var.toString(), SpecUtils.termToLatex(expr));
        }
        return map;
    }

    public Map<String,String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Term auxTerm;
        Map<String,String> map = new HashMap<String,String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                auxTerm = (Term) object;
                map.putAll(auxTerm.accept(this));
            }
        }
        return map;
    }
}